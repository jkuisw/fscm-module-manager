A list of all exported procedures of this module. The procedure names are the 
internal procedure names. Normally this is the same as the exported name, but 
thy can differ. Look into the detailed procedure documentation to get the export 
name of the procedure. The procedures listed in the table below are imported 
into the global environment with their export name.  
