;*******************************************************************************
; This part of the module provides procedures for interacting with the import
; tree. The import tree tracks the relations between all imported modules, their
; import names and their "child" modules. With the import tree the module 
; manager knows which module is imported with which import name and to which 
; module environment. 

;*******************************************************************************
; Module global variables.

; A list of registered (used) import names for the loaded modules. A list entry
; is also a list with four entries: 
;   (1) import-name - The import name, must be a symbol.
;   (2) module-name - The name of the module the import name is related to, must
;     be a string.
;   (3) module-version - The version of the module loaded, must be a valid
;     version string.
;   (4) child-modules - A list of the imported modules to the module 
;     environment, which are the modules this module depends on. An entry in 
;     the childs list is the same as the entry of the import-tree list. 
;     Therefore a tree structure is build.
;
; Example for an import-tree:
; ```scheme
; (list
;   (list 'fs "fscm-file-system-ops" "0.5.0" (list
;     (list 'utils "fscm-utils" "0.5.0" '())
;   ))
;   (list 'u "fscm-utils" "0.5.0" '())
;   (list 'pman "fscm-project-manager" "0.0.4" (list
;     (list 'utils "fscm-utils" "0.5.0" '())
;   ))
; )
; ```
(define import-tree '())

;*******************************************************************************
; Traverses the import tree and executes actions on specific positions and
; states of the import tree. See the actions parameter for further information.
; The procedure can operate in two different modes:  
; + `"find"` mode - When in the settings only the import-name and import-path
;   keys are passed. These twoe settings are mandatory in this mode. Than the
;   procedure will search for the module, identified by the import-path and
;   import-name. When found, than the module entry in the import tree will be
;   passed to the `"found-module-action"`. The actions `"add-entry-action"`,
;   `"matching-imported-action"` and `"other-imported-action"` will be ignored in
;   this mode.
; + `"add"` mode - When all settings are passed. In this mode all settings are
;   mandatory. Then the procedure will add a new entry for the module to the
;   import tree (when the action `"add-entry-action"` returns true, see below).
;   The action `"found-module-action"` will be ignored in this mode.
;
; Parameters:
;   settings - A settings key value pair list which must have the following
;     entries:  
;     + `import-path` - The import path of the module.
;     + `import-name` - The import name of the module.
;     + `module-name` - The module name.
;     + `module-version` - The module version.
;     + `version-range` - A valid version range for the module.
;   actions - A key value pair list with different actions. An action is a
;     procedure that will be called on specific positions and states of the
;     import tree. If a action is not passed, than it will be ignored. The
;     following actions are supported:  
;     + `path-not-found-action` - Will be called when the import path could not
;       be found in the import tree. The procedure will be called with the
;       following parameters:  
;       + `level-tree` - The remaining entries of the current subtree.
;       + `path` - The remaining entries of the import path.
;     + `add-entry-action` - Called when the position for a new entry is found.
;       The entry is than added only when the procedure returns true. If false
;       is returned by the procedure, than the entry will not be added. The
;       procedure will be called with the following parameters:
;       + `level-tree` - The remaining entries of the current subtree. If an
;         empty list, than this will be the first entry in the import tree.
;         If not, than there should be only one entry in the remaining
;         subtree.
;       + `path` - The remaining entries of the import path. Should be an
;         empty list at this point.
;     + `matching-imported-action` - Will be called if a matching entry,
;       according to the settings passed, is already in the import tree. The
;       procedure will be called with the following parameters:
;       + `entry` - The entry of the already imported module in the import
;         tree.
;     + `other-imported-action` - Will be called if the import name is already
;       used by another loaded module, which does not match with the settings
;       passed. The procedure will be called with the following parameters:
;       + `entry` - The entry of the already imported module in the import
;         tree.
;     + `found-module-action` - Will be called when in `"find"` mode and the
;       module, identified by the import-name and import-path, is found. The
;       procedure will be called with the following parameters:
;       + `entry` - The import tree entry of the found module.
(define (traverse-import-tree settings actions) (let (
    (entry '())
    (mode 'add)
    ; settings
    (import-path (map-get-value settings 'import-path))
    (import-name (map-get-value settings 'import-name))
    (module-name (map-get-value settings 'module-name))
    (module-version (map-get-value settings 'module-version))
    (version-range (map-get-value settings 'version-range))
    ; actions
    (path-not-found-action (map-get-value actions 'path-not-found-action))
    (add-entry-action (map-get-value actions 'add-entry-action))
    (matching-imported-action (map-get-value actions 'matching-imported-action))
    (other-imported-action (map-get-value actions 'other-imported-action))
    (found-module-action (map-get-value actions 'found-module-action))
  )
  ; Check parameters.
  (cond
    ((eq? import-path 'key-not-found)
      (error "Missing \"import-path\" in settings parameter list!" settings))
    ((eq? import-name 'key-not-found)
      (error "Missing \"import-name\" in settings parameter list!") settings)
    ((and
        (eq? module-name 'key-not-found)
        (eq? module-version 'key-not-found)
        (eq? version-range 'key-not-found)
      )
      ; Mode - "find": Find module by import name and import path.
      (set! mode 'find)
    )
    ; Mode - "add": Add entry to import tree.
    (else (cond
      ((eq? module-name 'key-not-found)
        (error "Missing \"module-name\" in settings parameter list!") settings)
      ((eq? module-version 'key-not-found)
        (error "Missing \"module-version\" in settings parameter list!")
          settings)
      ((eq? version-range 'key-not-found)
        (error "Missing \"version-range\" in settings parameter list!")
          settings)
    ))
  )

  ; Generate entry.
  (if (eq? mode 'add)
    (set! entry (list ; A import tree entry.
      import-name ; The import name of the module.
      module-name ; The name of the module.
      module-version ; The version of the module.
      '() ; The child imports of the module.
    ))
  )

  ; Traverse the import tree and execute the passed actions.
  (let traverse-tree (
      (level-tree import-tree)
      (path import-path)
    )
    (cond
      ((null? level-tree)
        (if (null? path) (begin
          ; Import-tree is empty, create first entry.
          (if (and (eq? mode 'add) (not (eq? add-entry-action 'key-not-found)))
            (if (add-entry-action level-tree path) (begin
              (set! import-tree (list entry))
            ))
          )
        ) (begin ; else
          ; Error, path not found.
          (if (not (eq? path-not-found-action 'key-not-found))
            (path-not-found-action level-tree path)
          )
        ))
      )
      ((null? path)
        (if (eq? import-name (caar level-tree)) (begin
          (cond
            ((eq? mode 'add)
              (if (and
                  (string=? (cadar level-tree) module-name)
                  (is-version-range-matching? (caddar level-tree) version-range)
                ) (begin
                ; Matching module already imported.
                (if (not (eq? matching-imported-action 'key-not-found))
                  (matching-imported-action (car level-tree))
                )
              ) (begin ; else
                ; Other module to same import name already imported.
                (if (not (eq? other-imported-action 'key-not-found))
                  (other-imported-action (car level-tree))
                )
              ))
            )
            ((eq? mode 'find)
              (if (not (eq? found-module-action 'key-not-found))
                (found-module-action (car level-tree))
              )
            )
          )
        ) (begin ; else
          ; Check if reached last element of level tree.
          (if (<= (length level-tree) 1) (begin
            ; Reached last element of level tree and import was not found.
            ; Therefore create an entry.
            (if (and
                (eq? mode 'add)
                (not (eq? add-entry-action 'key-not-found))
              )
              (if (add-entry-action level-tree path) (begin
                (set-cdr! level-tree (list entry))
              ))
            )
          ) (begin ; else
            ; Go to the next element in the level tree.
            (traverse-tree (cdr level-tree) path)
          ))
        ))
      )
      ((and
          (eq? mode 'add)
          (eq? import-name (caar level-tree))
          (string=? (cadar level-tree) module-name)
          (is-version-range-matching? (caddar level-tree) version-range)
        )
        ; Matching module already imported.
        (if (not (eq? matching-imported-action 'key-not-found))
          (matching-imported-action (car level-tree))
        )
      )
      ((eq? (car path) (caar level-tree)) (let (
          (childs (list-ref (car level-tree) 3))
          (next-path (cdr path))
        )
        (if (null? childs) (begin
          (if (null? next-path) (begin
            ; First child to the module, add entry.
            (if (and
                (eq? mode 'add)
                (not (eq? add-entry-action 'key-not-found))
              )
              (if (add-entry-action level-tree path) (begin
                (list-set! (car level-tree) 3 (list entry))
              ))
            )
          ) (begin ; else
            ; Error, path not found.
            (if (not (eq? path-not-found-action 'key-not-found))
              (path-not-found-action level-tree next-path)
            )
          ))
        ) (begin ; else
          ; Found import tree part, go to the next import tree level (which is
          ; child module imports entry of the current import tree entry).
          (traverse-tree childs next-path)
        ))
      ))
      ((> (length level-tree) 1)
        ; Import path not found, go to the next level tree entry.
        (traverse-tree (cdr level-tree) path)
      )
      (else
        ; Error, path not found.
        (if (not (eq? path-not-found-action 'key-not-found))
          (path-not-found-action level-tree path)
        )
      )
    )
  )
))

;*******************************************************************************
; Generates a list of the exports the module, identified by the import-name
; parameter, has exported.
;
; Parameters:
;   import-name - The import name to which the module was imported.
;   import-path - [optional, default: '()] The import path to which the module
;     with the passed import-name was passed. Default is the global import path.
;
; Returns: A list of symbols which are the names of the exported procedures.
(define (list-module-exports import-name . args) (let (
    (exported-procedure-names '())
    (exports-list #f)
    (module '())
    (module-name "")
    (module-version "")
    (import-path '())
  )
  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Import path parameter passed.
    (set! import-path (list-ref args 0))
    (if (and (not (null? import-path)) (not (pair? import-path)))
      (error "Invalid type of argument \"import-path\", must be a list!"
        import-path)
    )
  ))

  ; Check and convert import-path parameter.
  (if (not (null? import-path))
    (set! import-path (let loop (
        (remaining import-path)
      )
      (cond
        ((null? remaining) '())
        ((string? (car remaining))
          (cons (string->symbol (car remaining)) (loop (cdr remaining)))
        )
        ((symbol? (car remaining))
          (cons (car remaining) (loop (cdr remaining)))
        )
        (else (error
          "Parameter \"import-path\" must be a list of strings and/or symbols!"
          import-path
        ))
      )
    ))
  )

  ; Check and convert import-name parameter.
  (if (string? import-name) (set! import-name (string->symbol import-name)))
  (if (not (symbol? import-name))
    (error "Invalid type of argument \"import-name\" must be symbol or string!"
      import-name)
  )

  ; Get the module name and version form the import tree.
  (traverse-import-tree (list
      ; settings
      (list 'import-name import-name)
      (list 'import-path import-path)
    ) (list
      ; actions
      (list 'path-not-found-action (lambda (level-tree path)
        (error (format #f "~a\n~a ~a\n~a ~a\n"
          "Could not find import path in import tree!"
          "  -> level-tree: " level-tree
          "  -> path: " path
        ) import-path)
      ))
      (list 'found-module-action (lambda (entry)
        (set! module-name (cadr entry))
        (set! module-version (caddr entry))
      ))
    )
  )

  ; Check if found module.
  (if (or (string=? module-name "") (string=? module-version ""))
    (error (format #f "~a \"~a\" ~a \"~a\"!"
      "Could not find the module related to the passed import name"
      import-name "and import path" import-path
    ) import-name)
  )

  ; Retrieve the module settings.
  (set! module (get-module-settings module-name module-version))
  (if (eq? module 'key-not-found)
    (error (string-append
      "Failed to retrieve the module settings of the module \"" module-name
      "\" at version \"" module-version "\"!"
    ) module-name)
  )

  ; Get the module exports list.
  (set! exports-list (map-get-value module 'exports))

  ; Generate the export names list.
  (if (not (eq? exports-list 'key-not-found)) (begin
    (for-each (lambda (export-pair)
      (set! exported-procedure-names (append exported-procedure-names (list
        (format #f "~a/~a" import-name (car export-pair))
      )))
    ) exports-list)
  ) (begin ; else
    (error (string-append
      "Failed to retrieve the module exports of the module \"" module-name
      "\" at version \"" module-version "\"!"
    ) module)
  ))

  ; Return the names as symbols of the exported procedures.
  exported-procedure-names
))

;*******************************************************************************
; Prints the names of the exported procedures the module, identified by the
; import-name parameter, has exported.
;
; Parameters:
;   import-name - The import name to which the module was imported.
;   import-path - [optional, default: '()] The import path to which the module
;     with the passed import-name was passed. Default is the global import path.
(define (print-module-exports import-name . args) (let (
    (import-path '())
  )
  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Import path parameter passed.
    (set! import-path (list-ref args 0))
    (if (and (not (null? import-path)) (not (pair? import-path)))
      (error "Invalid type of argument \"import-path\", must be a list!"
        import-path)
    )
  ))

  ; Check and convert parameter.
  (if (string? import-name) (set! import-name (string->symbol import-name)))
  (if (not (symbol? import-name))
    (error "\"import-name\" parameter must be symbol or string!" import-name)
  )

  ; Print the exports.
  (for-each (lambda (proc-name)
    (display proc-name)
    (newline)
  ) (list-module-exports import-name import-path))
))

;*******************************************************************************
; Prints the module import tree.
(define (print-import-tree) (let (
    (childs '())
  )
  (let print-tree (
      (tree import-tree)
      (indent "")
    )
    (if (not (null? tree)) (begin
      (set! childs (list-ref (car tree) 3))

      (if (= (length tree) 1) (begin
        ; Last entry.
        (display (string-append indent "+---" (symbol->string (caar tree)) "\n"))
        (display (string-append indent "   |--- module name: " (cadar tree) "\n"))
        (display (string-append indent "   |--- module version: " (caddar tree) "\n"))
        (display (string-append indent "   +--- childs\n"))

        (if (null? childs) (begin
          (display (string-append indent "      +--- no childs\n"))
        ) (begin ; else
          (print-tree childs (string-append indent "      "))
        ))
      ) (begin ; else
        ; There are more than one entry.
        (display (string-append indent "|---" (symbol->string (caar tree)) "\n"))
        (display (string-append indent "|  |--- module name: " (cadar tree) "\n"))
        (display (string-append indent "|  |--- module version: " (caddar tree) "\n"))
        (display (string-append indent "|  +--- childs\n"))

        (if (null? childs) (begin
          (display (string-append indent "|     +--- no childs\n"))
          (print-tree (cdr tree) indent)
        ) (begin ; else
          (print-tree childs (string-append indent "|     "))
          (print-tree (cdr tree) indent)
        ))
      ))
    ))
  )

  ; Return empty string.
  ""
))

;*******************************************************************************
; Export public module manager procedures.
(export (list
  (list 'list-module-exports list-module-exports)
  (list 'print-module-exports print-module-exports)
  (list 'print-import-tree print-import-tree)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
