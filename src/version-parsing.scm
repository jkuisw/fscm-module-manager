;*******************************************************************************
; This file implements parsing and matching of semver strings. The semver 2.0.0
; specification for semantic versioning strings is fully implemented.  
;   -> see: http://semver.org/  
; For version string matching the version ranges are used.  
;   -> see: https://docs.npmjs.com/misc/semver  
; Except for "Advanced Range Syntax", all version-range features are
; supported and implemented.

(define decision-matrix:operator-index 0)
(define decision-matrix:operator-matrix-index 1)
(define decision-matrix:major-level-index 0)
(define decision-matrix:minor-level-index 1)
(define decision-matrix:patch-level-index 2)
(define decision-matrix:pre-release-level-index 3)
(define decision-matrix:lesser-than-index 0)
(define decision-matrix:equal-index 1)
(define decision-matrix:greater-than-index 2)
(define decision-matrix:wildcard-index 3)
(define version-decision-matrix (list
  ; na = not applicable
  (list '< (list
    ; decision matrix for comparison operator: "<"
    ; level\operator |      <     |      =     |      >     |      *     |
    ; --------------------------------------------------------------------
    ;          major |     OK     | next level |      X     | next level |
    (list             'match       'next        'no-match    'next       )
    ;          minor |     OK     | next level |      X     | next level |
    (list             'match       'next        'no-match    'next       )
    ;          patch |     OK     | next level |      X     |     OK     |
    (list             'match       'next        'no-match    'match      )
    ;    pre-release |     OK     |      X     |      X     |      X     |
    (list             'match       'no-match    'no-match    'na         )
    ; --------------------------------------------------------------------
  ))
  (list '<= (list
    ; decision matrix for comparison operator: "<="
    ; level\operator |      <     |      =     |      >     |      *     |
    ; --------------------------------------------------------------------
    ;          major |     OK     | next level |      X     | next level |
    (list             'match       'next        'no-match    'next       )
    ;          minor |     OK     | next level |      X     | next level |
    (list             'match       'next        'no-match    'next       )
    ;          patch |     OK     | next level |      X     |     OK     |
    (list             'match       'next        'no-match    'match      )
    ;    pre-release |     OK     |     OK     |      X     |      X     |
    (list             'match       'match       'no-match    'na         )
    ; --------------------------------------------------------------------
  ))
  (list '> (list
    ; decision matrix for comparison operator: ">"
    ; level\operator |      <     |      =     |      >     |      *     |
    ; --------------------------------------------------------------------
    ;          major |      X     | next level |     OK     | next level |
    (list             'no-match    'next        'match       'next       )
    ;          minor |      X     | next level |     OK     | next level |
    (list             'no-match    'next        'match       'next       )
    ;          patch |      X     | next level |     OK     |     OK     |
    (list             'no-match    'next        'match       'match      )
    ;    pre-release |      X     |      X     |     OK     |      X     |
    (list             'no-match    'no-match    'match       'na         )
    ; --------------------------------------------------------------------
  ))
  (list '>= (list
    ; decision matrix for comparison operator: ">="
    ; level\operator |      <     |      =     |      >     |      *     |
    ; --------------------------------------------------------------------
    ;          major |      X     | next level |     OK     | next level |
    (list             'no-match    'next        'match       'next       )
    ;          minor |      X     | next level |     OK     | next level |
    (list             'no-match    'next        'match       'next       )
    ;          patch |      X     | next level |     OK     |     OK     |
    (list             'no-match    'next        'match       'match      )
    ;    pre-release |      X     |     OK     |     OK     |      X     |
    (list             'no-match    'match       'match       'na         )
    ; --------------------------------------------------------------------
  ))
  (list '= (list
    ; decision matrix for comparison operator: "="
    ; level\operator |      <     |      =     |      >     |      *     |
    ; --------------------------------------------------------------------
    ;          major |      X     | next level |      X     | next level |
    (list             'no-match    'next        'no-match    'next       )
    ;          minor |      X     | next level |      X     | next level |
    (list             'no-match    'next        'no-match    'next       )
    ;          patch |      X     | next level |      X     |     OK     |
    (list             'no-match    'next        'no-match    'match      )
    ;    pre-release |      X     |     OK     |      X     |      X     |
    (list             'no-match    'match       'no-match    'na         )
    ; --------------------------------------------------------------------
  ))
))

;*******************************************************************************
; Parses the given version or version-range string and returns the parsed data
; in a list of the following format:  
; The parsed data is a list of or logical linkage lists, like:  
; ```scheme
; (list  
;   (list ...) ; or logical linkage list  
;   (list ...) ; or logical linkage list  
;   ...  
; )  
; ```
; If a version-range string is passed, than each comparator or comparators
; joined by spaces (= "and logical linkage") which is linked through "||"
; characters (= "or logical linkage") creates a new "or logical linkage
; list". If a version string is passed than only one "or logical linkage list"
; is created, otherwise it would be a version-range string.
; Each "or logical linkage list" is a list of "and logical linkage lists",
; which are the comparators seperated by spaces. Again for a version string
; there can only exist one "and logical linkage list", otherwise it would be a
; version range. So a "or logical linkage list" looks like:  
; ```scheme
; (list  
;   (list ...) ; and logical linkage list  
;   (list ...) ; and logical linkage list  
;   ...  
; )  
; ```
; A "and logical linkage list" is a parsed comparator which is implemented as a
; key value map with the following keys:  
; + `'operator` - The comparison operator, which can be one of the
;   following values: `""`, `">"`, `">="`, `"<"`, `"<="`, `"="`  
;   Whereby `""` is equal to `"="`. For a version string (not a version-range) this
;   is always `""`.
; + `'major` - The major version as string (can also be `"*"`).
; + `'minor` - The minor version as string (can also be `"*"`).
; + `'patch` - The patch version as string (can also be `"*"`).
; + `'pre-release` - A list of strings, which are the identifiers of the
;   pre-release version.
; + `'metadata` - A list of strings, which are the identifiers of the
;   build metadata.
;
; For the specification of a version, see: http://semver.org/  
; For the definition of a version-range, comparator, operator and other
; keywords, see: https://docs.npmjs.com/misc/semver  
; > NOTE: The "Advanced Range Syntax" of a version-range is not supported.
;
; Parameters:
;   version - The version or version-range string to parse.
;   print-parse-result - [optional, default: #f] True when the parse result
;     should be printed, false otherwise.
;
; Returns: The parsed version / version-range as list, see above.
(define (parse-version version . args) (letrec (
    (operator "")
    (major "")
    (minor "")
    (patch "")
    (pre-release '())
    (metadata '())
    (curr-identifier "")
    (state 'comp-op)
    (last-state 'comp-op)
    (tmp-str "")
    (parse-result '())
    (digit-count 0)
    (pipe-count 0)
    (no-new-version-found #f)
    (print-parse-result #f)
    (change-state-to (lambda (new-state)
      (set! last-state state)
      (set! state new-state)
    ))
    (append-version-to-parse-result (lambda ()
      (if (> (string-length curr-identifier) 0) (begin
        (cond
          ((or
              (eqv? state 'pre-release)
              (and (eqv? state 'next-version) (eqv? last-state 'pre-release))
            )
            (if (null? pre-release)
              (set! pre-release (list curr-identifier))
              (append-to-list pre-release curr-identifier)
            )
          )
          ((or
              (eqv? state 'metadata)
              (and (eqv? state 'next-version) (eqv? last-state 'metadata))
            )
            (if (null? metadata)
              (set! metadata (list curr-identifier))
              (append-to-list metadata curr-identifier)
            )
          )
        )

        (set! curr-identifier "")
      ))
      
      (if (<= (string-length major) 0) (set! major "*"))
      (if (<= (string-length minor) 0) (set! minor "*"))
      (if (<= (string-length patch) 0) (set! patch "*"))
      
      (if (not no-new-version-found)
        (let (
            (last-or '())
            (entry (list
              (list 'operator (string->symbol operator))
              (list 'major major)
              (list 'minor minor)
              (list 'patch patch)
              (list 'pre-release pre-release)
              (list 'metadata metadata)
            ))
          )
          ; char-list = null => reached end of version string.
          (if (null? parse-result) (begin
            ; First entry in parse-result.
            (set! parse-result (list
              ; or logical linkage list, entries in this list ar logical and linked.
              (list entry) ; version entry
            ))
          ) (begin ; else
            ; Not first entry in parse-result, therefore get last "or logical
            ; linkage list" and add the entry to it.
            (set! last-or (car (list-tail
              parse-result
              (- (length parse-result) 1)
            )))
            (append-to-list last-or entry)
          ))
        )
      )
    ))
  )
  ; Check version parameter.
  (if (symbol? version) (set! version (symbol->string version)))
  (if (not (string? version)) 
    (error "Invalid parameter \"version\", must be a string!" version)
  )
  (if (<= (string-length version) 0) (set! version "*.*.*"))
  
  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Print parse result parameter passed.
    (set! print-parse-result (list-ref args 0))
    (if (not (boolean? print-parse-result))
      (error
        "Invalid type of parameter \"print-parse-result\", must be a boolean!"
        print-parse-result
      )
    )
  ))

  ; Parse version string by looping over all characters of the version string.
  (let next-char ((char-list (string->list version)))
    (if (not (null? char-list)) (let (
        (curr-char (car char-list))
        (is-op-char #f)
        (is-wildcard-version #f)
        (is-identifier-char #f)
        (pass-same-char-list #f)
      )

      ; Check if char is a wildcard version character.
      (set! is-wildcard-version (or
        (char=? curr-char #\*)
        (char=? curr-char #\x)
        (char=? curr-char #\X)
      ))

      ; Only got to state comp-op if there is any comparison operator.
      (if (eqv? state 'comp-op) (begin
        ; Check if char is an comparison operator.
        (set! is-op-char (or
          (char=? curr-char #\<)
          (char=? curr-char #\>)
          (char=? curr-char #\=)
        ))

        (if (not is-op-char) (begin
          (set! digit-count 0)
          (change-state-to 'major)
        ))
      ))

      (cond
        ; ----------------------------------------------------------------------
        ((eqv? state 'comp-op) (begin
          (cond
            (is-op-char (begin
              (set! operator (string-append operator (make-string 1 curr-char)))
            ))
            (else (error "Invlaid version string!" version)) ; This should not occur!
          )
        ))
        ; ----------------------------------------------------------------------
        ((eqv? state 'major) (begin
          (cond
            ; is wildcard char?
            ((and (< digit-count 1) is-wildcard-version) (begin
              (set! major "*")
              (set! digit-count 1)
            ))
            ; is major-minor-version seperator char?
            ((and (>= digit-count 1) (char=? curr-char #\.)) (begin
              ;(display "point\n")
              (set! digit-count 0)
              (change-state-to 'minor)
            ))
            ; is numeric (= major version) char?
            ((and (not (eqv? major "*")) (char-numeric? curr-char)) (begin
              (set! major (string-append major (make-string 1 curr-char)))
              ;(display "is a number, major = ") (display major) (newline)
              (set! digit-count (+ digit-count 1))
              ;(display "digit-count = " ) (display digit-count) (newline)
            ))
            (else (error "Invlaid version string!" version))
          )
        ))
        ; ----------------------------------------------------------------------
        ((eqv? state 'minor) (begin
          (cond
            ; is wildcard char?
            ((and (< digit-count 1) is-wildcard-version) (begin
              (set! minor "*")
              (set! digit-count 1)
            ))
            ; is minor-patch-version seperator char?
            ((and (>= digit-count 1) (char=? curr-char #\.)) (begin
              (set! digit-count 0)
              (change-state-to 'patch)
            ))
            ; is numeric (= minor version) char?
            ((and (not (eqv? minor "*")) (char-numeric? curr-char)) (begin
              (set! minor (string-append minor (make-string 1 curr-char)))
              (set! digit-count (+ digit-count 1))
            ))
            (else (error "Invlaid version string!" version))
          )
        ))
        ; ----------------------------------------------------------------------
        ((eqv? state 'patch) (begin
          (cond
            ; is wildcard char?
            ((and (< digit-count 1) is-wildcard-version) (begin
              (set! patch "*")
              (set! digit-count 1)
            ))
            ; is space char?
            ((and (>= digit-count 1) (char=? curr-char #\space)) (begin
              (set! digit-count 1)
              (change-state-to 'next-version)
            ))
            ; is pre-release char?
            ((and (>= digit-count 1) (char=? curr-char #\-)) (begin
              (set! digit-count 0)
              (set! curr-identifier "")
              (change-state-to 'pre-release)
            ))
            ; is metadata char?
            ((and (>= digit-count 1) (char=? curr-char #\+)) (begin
              (set! digit-count 0)
              (set! curr-identifier "")
              (change-state-to 'metadata)
            ))
            ; is numeric (= patch version) char?
            ((and (not (eqv? minor "*")) (char-numeric? curr-char)) (begin
              (set! patch (string-append patch (make-string 1 curr-char)))
              (set! digit-count (+ digit-count 1))
            ))
            (else (error "Invlaid version string!" version))
          )
        ))
        ; ----------------------------------------------------------------------
        ((eqv? state 'pre-release) (begin
          ; Check if current character is an identifier character.
          (set! is-identifier-char (or
            (char-numeric? curr-char)
            (char-alphabetic? curr-char)
            (char=? curr-char #\-)
          ))

          (cond
            ; is space char?
            ((and (>= digit-count 1) (char=? curr-char #\space)) (begin
              (set! digit-count 1)
              (change-state-to 'next-version)
            ))
            ; is identifier seperator char?
            ((and (>= digit-count 1) (char=? curr-char #\.)) (begin
              (if (<= (string-length curr-identifier) 0)
                (error "Invlaid version string!" version)
              )

              (if (null? pre-release)
                (set! pre-release (list curr-identifier))
                (append-to-list pre-release curr-identifier)
              )

              (set! digit-count 0)
              (set! curr-identifier "")
            ))
            ; is metadata char?
            ((and (>= digit-count 1) (char=? curr-char #\+)) (begin
              (if (<= (string-length curr-identifier) 0)
                (error "Invlaid version string!" version)
              )

              (if (null? pre-release)
                (set! pre-release (list curr-identifier))
                (append-to-list pre-release curr-identifier)
              )

              (set! digit-count 0)
              (set! curr-identifier "")
              (change-state-to 'metadata)
            ))
            ; is identifier char?
            (is-identifier-char (begin
              (set! curr-identifier (string-append
                curr-identifier
                (make-string 1 curr-char)
              ))
              (set! digit-count (+ digit-count 1))
            ))
            (else (error "Invlaid version string!" version))
          )
        ))
        ; ----------------------------------------------------------------------
        ((eqv? state 'metadata) (begin
          ; Check if current character is an identifier character.
          (set! is-identifier-char (or
            (char-numeric? curr-char)
            (char-alphabetic? curr-char)
            (char=? curr-char #\-)
          ))

          (cond
            ; is space char?
            ((and (>= digit-count 1) (char=? curr-char #\space)) (begin
              (set! digit-count 1)
              (change-state-to 'next-version)
            ))
            ; is identifier seperator char?
            ((and (>= digit-count 1) (char=? curr-char #\.)) (begin
              (if (<= (string-length curr-identifier) 0)
                (error "Invlaid version string!" version)
              )

              (if (null? metadata)
                (set! metadata (list curr-identifier))
                (append-to-list metadata curr-identifier)
              )

              (set! digit-count 0)
              (set! curr-identifier "")
            ))
            ; is identifier char?
            (is-identifier-char (begin
              (set! curr-identifier (string-append
                curr-identifier
                (make-string 1 curr-char)
              ))
              (set! digit-count (+ digit-count 1))
            ))
            (else (error "Invlaid version string!" version))
          )
        ))
        ; ----------------------------------------------------------------------
        ((eqv? state 'next-version) (begin
          (if (= digit-count 1) (begin
            ; Add parsed version to parse result and reset variables.
            (append-version-to-parse-result)
            (set! pipe-count 0)
            (set! no-new-version-found #t)
          ))

          (cond
            ; is space char before or pipes?
            ((and (= pipe-count 0) (char=? curr-char #\space))
              (set! digit-count (+ digit-count 1))
            )
            ; is space chat after or pipes?
            ((and (= pipe-count 2) (char=? curr-char #\space))
              (set! digit-count (+ digit-count 1))
            )
            ; is or pipe char?
            ((char=? curr-char #\|)
              (set! pipe-count (+ pipe-count 1))
              (set! digit-count (+ digit-count 1))
            )
            ; is new version char?
            ((and
                (not (char=? curr-char #\space))
                (not (char=? curr-char #\|))
                (or (= pipe-count 0) (= pipe-count 2))
                (>= digit-count 1)
              )
              ; Found beginning of next version.
              (if (= pipe-count 2) (begin
                ; Create new "or logical linkage list".
                (append-to-list parse-result (list '()))
              ))

              ; Initialize prsing for next version.
              (set! operator "")
              (set! major "")
              (set! minor "")
              (set! patch "")
              (set! pre-release '())
              (set! metadata '())
              (set! curr-identifier "")
              (set! digit-count 0)
              (set! pipe-count 0)
              (set! no-new-version-found #f)
              (change-state-to 'comp-op)

              ; Start parsing next version.
              (set! pass-same-char-list #t)
            )
            (else (error "Invlaid version string!" version))
          )
        ))
      )

      ; Parse next character.
      (if pass-same-char-list
        (next-char char-list)
        (next-char (cdr char-list))
      )
    ) (begin ; else
      (append-version-to-parse-result)
    ))
  )

  ; Print parse result.
  (if print-parse-result (begin
    (let (
        (i 1)
        (len (length parse-result))
      )
      (display (format #f "parsed version string \"~a\":\n" version))
      (display "  -> or:\n")
      (for-each (lambda (ands)
        (display "    + and:\n")
        (for-each (lambda (v)
          (display "      - ") (display v) (newline)
        ) ands)
        (if (< i len) (display "  -> or:\n"))
        (set! i (+ i 1))
      ) parse-result)
    )
  ))

  ; Return the result map.
  parse-result
))

;*******************************************************************************
; Calculates a decision according to the given versions. Each entry of the
; level-decisions list contains a decision result for an specific boolean
; operator. The boolean operators are applied to the v-val and r-val. The result
; of the operator that evaluates to true is returned.
;
; Parameters:
;   v-val - The version value.
;   r-val - The version range value.
;   level-decisions - The list of decisions for the current level. This is one
;     entry of the version decision matrix and the list contains decision
;     results for the following operations in the following order:
;       1. operator: `"<"`, lesser than
;       2. operator: `"="`, equal to
;       3. operator: `">"`, greater than
;       4. operator: `"*"`, wildcard
;
; Returns: The result of the decision.
(define (calc-level-decision v-val r-val level-decisions)
  (if (string=? r-val "*") (begin
    (list-ref level-decisions decision-matrix:wildcard-index)
  ) (begin ; else
    (if (not (number? v-val)) (set! v-val (string->number v-val)))
    (if (not (number? r-val)) (set! r-val (string->number r-val)))
    (cond
      ((< v-val r-val)
        (list-ref level-decisions decision-matrix:lesser-than-index)
      )
      ((= v-val r-val)
        (list-ref level-decisions decision-matrix:equal-index)
      )
      ((> v-val r-val)
        (list-ref level-decisions decision-matrix:greater-than-index)
      )
    )
  ))
)

;*******************************************************************************
; Calculates the decision for the two pre-release versions. Each entry of the
; pre-release-decisions list contains a decision result for an specific boolean
; operator. The `'*` operator is ignored, because it is not applicable to a
; pre-release version. The precedence between the two given pre-release versions
; will be calculated according to the semantic versioning specification 2.0.0,
; see: http://semver.org/#spec-item-11
; With the precedence result the decision can be returned from the
; pre-release-decisions list.
;
; Parameters:
;   v-val - The version value.
;   r-val - The version range value.
;   pre-release-decisions - The list of decisions for the pre-release level.
;     This is one entry of the version decision matrix and the list contains
;     decision results for the following operations in the following order:
;       1. operator: `"<"`, lesser than
;       2. operator: `"="`, equal to
;       3. operator: `">"`, greater than
;       4. operator: `"*"`, wildcard (ignored by pre-release versions)
;
; Returns: The result of the decision.
(define (calc-pre-release-decision v-val r-val pre-release-decisions)
  (let identifiers-loop (
      (v-identifier v-val)
      (r-identifier r-val)
    )
    (cond
      ((and (null? v-identifier) (not (null? r-identifier)))
        ; v-val < r-val
        (list-ref pre-release-decisions decision-matrix:lesser-than-index)
      )
      ((and (null? r-identifier) (not (null? v-identifier)))
        ; v-val > r-val
        (list-ref pre-release-decisions decision-matrix:greater-than-index)
      )
      ((and (null? v-identifier) (null? r-identifier))
        ; v-val = r-val
        (list-ref pre-release-decisions  decision-matrix:equal-index)
      )
      ((string=? (car v-identifier) (car r-identifier))
        ; Go to the next identifier
        (identifiers-loop (cdr v-identifier) (cdr r-identifier))
      )
      ((and ; Check if both identifiers are numbers.
          (string->number (car v-identifier))
          (string->number (car r-identifier))
        )
        (if (>
            (string->number (car v-identifier))
            (string->number (car r-identifier))
          ) (begin
          ; v-val > r-val
          (list-ref pre-release-decisions decision-matrix:greater-than-index)
        ) (begin ; else
          ; v-val < r-val
          (list-ref pre-release-decisions decision-matrix:lesser-than-index)
        ))
      )
      (else (if (string>? (car v-identifier) (car r-identifier)) (begin
          ; v-val > r-val
          (list-ref pre-release-decisions decision-matrix:greater-than-index)
        ) (begin ; else
          ; v-val < r-val
          (list-ref pre-release-decisions decision-matrix:lesser-than-index)
        ))
      )
    )
  )
)

;*******************************************************************************
; Checks if the comparator matches the given version.
;
; Parameters:
;   version - A parsed version, see the description of the `parse-version`
;     procedure.
;   comparator - A parsed comparator, see the description of the `parse-version`
;     procedure.
;
; Returns: True if comparator matches the version, false otherwise.
(define (is-comparator-matching? version comparator) (let (
    (version-major (string->number (map-get-value version 'major)))
    (version-minor (string->number (map-get-value version 'minor)))
    (version-patch (string->number (map-get-value version 'patch)))
    (version-pre-release (map-get-value version 'pre-release))
    (operator (map-get-value comparator 'operator))
    (comparator-major (map-get-value comparator 'major))
    (comparator-minor (map-get-value comparator 'minor))
    (comparator-patch (map-get-value comparator 'patch))
    (comparator-pre-release (map-get-value comparator 'pre-release))
    (result 'invalid)
  )
  ; Check if comparator is valid.
  (if (not (and
      (>= (length comparator) 4)
      (not (eqv? operator 'key-not-found))
      (not (eqv? comparator-major 'key-not-found))
      (not (eqv? comparator-minor 'key-not-found))
      (not (eqv? comparator-patch 'key-not-found))
      (not (eqv? comparator-pre-release 'key-not-found))
      (or (null? comparator-pre-release) (pair? comparator-pre-release))
    )) (begin
    (error "The passed comparator is not valid!" comparator)
  ))

  ; If there is no comparison operator, set it to "'=".
  (if (eqv? (string->symbol "") operator)
    (set! operator '=)
  )

  ; Loop through the entries for each comparison operator.
  (let next-comp-op-entry (
      (comp-op-entries-list version-decision-matrix)
    ) (let (
      (comp-op-entry '())
      (comp-op-matrix '())
      (comp-op-entry-operator 'invalid)
    )
    (if (null? comp-op-entries-list)
      ; Reached end of list, this should not occur!
      (error "Unknown comparison operator!" operator)
    )

    (set! comp-op-entry (car comp-op-entries-list))
    (set! comp-op-entry-operator
      (list-ref comp-op-entry decision-matrix:operator-index)
    )

    (if (eqv? operator comp-op-entry-operator) (begin
      ; Found entry for requested comparison operator.
      (set! comp-op-matrix
        (list-ref comp-op-entry decision-matrix:operator-matrix-index)
      )

      ; Level: major
      (set! result (calc-level-decision
        version-major comparator-major
        (list-ref comp-op-matrix decision-matrix:major-level-index)
      ))

      ; Level: minor
      (if (eqv? result 'next) (begin
        (set! result (calc-level-decision
          version-minor comparator-minor
          (list-ref comp-op-matrix decision-matrix:minor-level-index)
        ))
      ))

      ; Level: patch
      (if (eqv? result 'next) (begin
        (set! result (calc-level-decision
          version-patch comparator-patch
          (list-ref comp-op-matrix decision-matrix:patch-level-index)
        ))
      ))

      ; Level: pre-release
      (if (eqv? result 'next) (begin
        (set! result (calc-pre-release-decision
          version-pre-release comparator-pre-release
          (list-ref comp-op-matrix decision-matrix:pre-release-level-index)
        ))
      ))
    ) (begin ; else
      ; Comparison operator of current comp-op-entry is not the requested,
      ; therefore go to the next entry.
      (next-comp-op-entry (cdr comp-op-entries-list))
    ))
  ))

  (cond
    ; Return true when the comparator matches the version.
    ((eqv? result 'match) #t)
    ; Return false when the comparator does not match the version.
    ((eqv? result 'no-match) #f)
    ; Throw an error for all other values of result, because 'match and
    ; 'no-match are the only valid values of result at this point.
    (else (error (format #f "Failed to compare \"~a\" with \"~a\"!"
      version comparator
    ) result))
  )
))

;*******************************************************************************
; Checks if the version matches the version-range.
;
; Parameters:
;   version - A specific version string.
;   version-range - A version range string.
;
; Returns: True if the version matches the version-range, false otherwise.
(define (is-version-range-matching? version version-range) (let (
    (parsed-version (parse-version version))
    (parsed-version-range (parse-version version-range))
  )
  
  ; Check if version is valid.
  (if (not (and
      (pair? parsed-version)
      (= (length parsed-version) 1)
      (= (length (car parsed-version)) 1)
      (>= (length (caar parsed-version)) 3)
      (let (
          (major (map-get-value (caar parsed-version) 'major))
          (minor (map-get-value (caar parsed-version) 'minor))
          (patch (map-get-value (caar parsed-version) 'patch))
          (pre-release (map-get-value (caar parsed-version) 'pre-release))
        )
        (and
          (not (eqv? major 'key-not-found))
          (not (eqv? minor 'key-not-found))
          (not (eqv? patch 'key-not-found))
          (not (eqv? pre-release 'key-not-found))
          (string->number major)
          (string->number minor)
          (string->number patch)
          (or (null? pre-release) (pair? pre-release))
        )
      )
    )) (begin
    (error
      "Version must be a valid specific version and no version range!"
      parsed-version
    )
  ))

  ; Check if version range is valid.
  (if (not (and
      (pair? parsed-version-range)
      (>= (length parsed-version-range) 1)
      (>= (length (car parsed-version-range)) 1)
    )) (begin
    (error "Version range is invalid!" parsed-version-range)
  ))
  
  ; Loop through comparators and link them with the corresponding logical
  ; linkage operator to calculate if the version-range matches with the version.
  (let or-linkage-loop ((or-list parsed-version-range))
    (if (null? or-list)
      #f ; End of or linkage list.
      (or ; link result of each or-list entry with or operator
        (or-linkage-loop (cdr or-list)) ; next or-list entry
        (let and-linkage-loop ((and-list (car or-list))) ; comparator loop
          (if (null? and-list)
            #t ; End of and linkage list (comparator list).
            (and ; link result of each comparator matching with and operator
              (and-linkage-loop (cdr and-list)) ; next and-list entry (comparator)
              (is-comparator-matching? (caar parsed-version) (car and-list))
            )
          )
        )
      )
    )
  )
))

;*******************************************************************************
; Export public module manager procedures.
(export (list
  (list 'is-version-range-matching? is-version-range-matching?)
  (list 'parse-version parse-version)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
