;*******************************************************************************
; Provides the core functionality of the module mananger, which is:  
;
; + adding module paths to find fluent scheme modules
; + search for modules in the added module paths
; + read the module definitions of the modules
; + load modules when requested and needed
; + import modules to source files
; + print information about the known and loaded modules

;*******************************************************************************
; Module global variables.
(define module-paths (list '()))
(define should-refresh-modules-list #t)

; Contains the list of currently known modules, found by the module paths when
; executing the refresh-modules-list procedure. A list entry represents a
; specific module at a specific version and is a list of key value pairs, which
; are:
;   name - The name of the module.
;   version - The version of the module.
;   supported-platforms - The platforms this module supports.
;   dependencies - Other modules this module depends on, will be loaded before
;     this module is loaded.
;   src-dirs - Additional source code directories to search for *.scm files when
;     loading the module.
;   loaded - True if the module is already loaded.
;   path - The module path.
;   TODO
(define modules (list '()))

;*******************************************************************************
; Actually loads a module, which means: Find the module in the modules list,
; retrieve the module settings. Find all source files in the module path and
; module source files. Finally load all source files and set the loaded flag of
; the module to true.
;
; Parameters:
;   import-name - The import name of the module, must be a symbol.
;   name - The module name, must be a string.
;   version-range - [optional, default: ""] The version range to match against.
;   parent-env - [optional, default: user-initial-environment] The parent
;     environment of the environment where the module will be loaded to.
(define (load-module import-name name . args) (let (
    (module '())
    (module-path "")
    (module-version "")
    (src-path "")
    (version-range "")
    (force-load #f)
    (loaded #f)
    (new-import-name #f)
    (source-files-list '())
    (parent-env '())
    (module-env '())
    (module-exports '())
    (objects-to-inject '())
    (reg-imp '())
    (parent-env-last-var-name "")
    (module-import-path '())
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Version range parameter passed.
    (set! version-range (list-ref args 0))
  ))

  (if (> (length args) 1) (begin
    ; Force load parameter passed.
    (set! force-load (list-ref args 1))
  ))

  (if (> (length args) 2) (begin
    ; Parent environment parameter passed.
    (set! parent-env (list-ref args 2))
  ) (begin ; else
    (set! parent-env user-initial-environment)
  ))

  ; Retrieve module import path.
  (if (and (environment? parent-env) (> (length (caar parent-env)) 0)) (begin
    (set! parent-env-last-var-name
      (list-ref (caar parent-env) (- (length (caar parent-env)) 1))
    )
    (if (eq? parent-env-last-var-name 'module-import-path)
      (set! module-import-path
        (list-ref (cdar parent-env) (- (length (cdar parent-env)) 1))
      )
    )
  ))
  
  ; Get the module settings.
  (if (string=? version-range "")
    (set! module (get-module-settings name))
    (set! module (get-module-settings name version-range))
  )

  (if (not (null? module)) (begin
    ; Check if module is already loaded when force-load is false.
    (if (not force-load) (begin
      (set! loaded (map-get-value module 'loaded))
    ))

    ; Get module path and version.
    (set! module-path (map-get-value module 'path))
    (set! module-version (map-get-value module 'version))

    ; Check if the module is already loaded into the parent environment. If not,
    ; register the module to the import treee.
    (traverse-import-tree (list
        ; settings
        (list 'import-path module-import-path)
        (list 'import-name import-name)
        (list 'module-name name)
        (list 'module-version module-version)
        (list 'version-range version-range)
      ) (list
        ; actions
        (list 'path-not-found-action (lambda (level-tree path)
          (error (format #f "~a\n~a ~a\n~a ~a\n"
            "Could not find import path in import tree!"
            "  -> level-tree: " level-tree
            "  -> path: " path
          ) module-import-path)
        ))
        (list 'add-entry-action (lambda (level-tree path) (begin
          ; Module not loaded in the parent environment.
          (set! new-import-name #t)
          #t ; Return true to register the module to the import tree.
        )))
        (list 'matching-imported-action (lambda (entry)
          ; Module already loaded into the parent environment.
          (set! new-import-name #f)
        ))
        (list 'other-imported-action (lambda (entry)
          (if (not force-load)
            (error (format #f (string-append
              "Could not import module \"~a\" with version-range \"~a\" to \""
              "~a\" at import path \"~a\", because there is already the module "
              "\"~a\" at version \"~a\" imported to \"~a\", that does not "
              "match with the module to import.")
              name version-range import-name module-import-path (cadr entry)
              (caddr entry) import-name
            ) import-name)
          )
        ))
      )
    )
    
    ; Load the module.
    (if (not loaded) (begin
      ; Get the objects to inject to the module environment.
      (set! objects-to-inject (map-get-value module 'objects-to-inject))
      (if (eqv? objects-to-inject 'key-not-found) (set! objects-to-inject '()))

      ; Create the module environment.
      (set! module-env (create-module-enviroment
        module-import-path
        import-name
        parent-env
        name
        objects-to-inject
        ; The export procedure, needed by the module to export public
        ; procedures.
        ;
        ; Parameters:
        ;   exports - A name value pair list with the procedures of the module
        ;     to export. The name must be a symbol and the value a procedure.
        ;     Normally the name should be the same as the procedure name, but
        ;     this is not mandatory. e.g.:
        ;       (list (list 'proc1 proc1) (list 'proc2 proc2) ...)
        (lambda (exports)
          (set! module-exports (append module-exports exports))
        )
      ))

      ; Now load the module.
      ; First load source files in the module root.
      (load-source-files-of-directory module-path module-env)

      ; Loop through all module source directories, find the *.scm files in that
      ; directories and load them all. The directories existance was checked
      ; when loading the module definition.
      ; ASSUMPTION: The directories did not change (deleted or renamed) since
      ;   the module definition was read. If so, than the module definition
      ;   has to be reloaded.
      (for-each (lambda (src-dir)
        (set! src-path (format #f "~a/~a" module-path src-dir))
        (load-source-files-of-directory src-path module-env)
      ) (map-get-value module 'src-dirs))

      ; Export the module exports to the parent environment.
      (for-each (lambda (export-pair)
        (eval (list 'define
          (string->symbol (format #f "~a/~a" import-name (car export-pair)))
          (cadr export-pair)
        ) parent-env)
      ) module-exports)

      ; Save module exports.
      (map-set-value module 'exports module-exports)

      ; Set the loaded flag of the module.
      (if (not (set-module-loaded name module-version))
        (error (format #f "~a \"~a\" ~a \"~a\" ~a"
          "Failed to set the module" name "at version" module-version
          "loaded. Because the module was not found in the modules list."
        ))
      )
    ) (begin ; else
      ; Module already loaded, therefore check if exports need to be loaded.
      (if new-import-name (begin
        ; Exports need to be loaded.
        (set! module-exports (map-get-value module 'exports))
        (if (eq? module-exports 'key-not-found) (begin
          (error (format #f "~a \"~a\" ~a \"~a\"!"
            "Failed to retrieve the module exports from the module" name
            "at version" module-version
          ) module)
        ) (begin ; else
          ; Export the module exports with the new import-name to the parent
          ; environment.
          (for-each (lambda (export-pair)
            (eval (list 'define
              (string->symbol (format #f "~a/~a" import-name (car export-pair)))
              (cadr export-pair)
            ) parent-env)
          ) module-exports)
        ))
      ))
    ))

    #t ; Return true.
  ) (begin ; else
    (error (format #f "~a \"~a\" ~a \"~a\", ~a\n~a\n"
      "Could not load the module" name "with the version range" version-range
      "because it`s not known by the module manager."
      "Maybe missing a module path?") name)

    #f ; Return false.
  ))
))

;*******************************************************************************
; Reads the dependencies (module definitions) of a module.
;
; Parameters:
;   basedir - The path to the directory of the module which dependencies should
;     be read.
;   dependencies - The list of dependencies to read, see the file
;     "moduledef.scm.template" for the list format of this parameter.
;   dep-path - The dependency path, see the description of the procedure
;   "read-module-definition" for an explanation of this parameter.
(define (read-dependent-modules basedir dependencies dep-path) (let (
    (curr-dependency '())
    (curr-module-path "")
    (curr-module-name "")
    (curr-module-version-range "")
    (found-match #f)
  )
  ; Loop through module dependencies.
  (let next-dependency ((deps-list dependencies))
    (if (or (null? deps-list) (null? (list-ref deps-list 0))) (begin
      ; Reached end of dependencies list, therefore return.
      #t
    ) (begin ; else
      (set! curr-dependency (car deps-list))
      (set! curr-module-name (list-ref curr-dependency 0))
      (set! curr-module-version-range (list-ref curr-dependency 1))
      (set! curr-module-path (format #f "~a/~a" basedir
        (list-ref curr-dependency 2) ; Relative module path.
      ))

      ; Loop through dependency path to check if matching module is`nt already
      ; in the dependency path.
      (set! found-match (let next-dep-path-entry ((dp dep-path))
        (cond
          ; Reached last element of the list, therefore no matching module
          ; found.
          ((null? dp) #f)
          ; Check the current entry and if it`s matching return true.
          ((and
            (string=? curr-module-name (list-ref (car dp) 0))
            (is-version-range-matching
              (list-ref (car dp) 1) ; version of the module in the dependency path
              curr-module-version-range
            )
          ) #t)
          ; No match found and list is not empty, therefore go to the next
          ; element.
          (else (next-dep-path-entry (cdr dp)))
        )
      ))

      ; Check if the current dependency module matches with any known module.
      (set! found-match (or found-match
        (is-known-module? curr-module-name curr-module-version-range)
      ))

      (if (not found-match) (begin
        ; Did not found a matching module in the dependency path, therefore
        ; read the module definition.
        (read-module-definition curr-module-path dep-path)
      ))

      ; Go to the next module dependency.
      (next-dependency (cdr deps-list))
    ))
  )
  (newline) (newline)
))

;*******************************************************************************
; This procedure reads the module definition of the given module path and it's
; dependencies.
;
; Parameters:
;   path - The module path, which contains the module definition file.
;   dependency-path - [optional, default: '()] The dependency path for the
;     module to load. This is a list of name - version - pairs of the modules
;     that are depending on this module. Example:  
;     ```scheme
;     (list 
;       (list "module1" "2.5.3") 
;       (list "module2" "1.1.3")
;     )
;     ```
;     This means: The first call of this procedure was to read the module
;     definition of the module "module1", which depends on the module "module2".
;     So the procedure is called to read the module definition of the module
;     "module2", which dependes on the current module, so the procedure is
;     called to read the module definition of the current module.
(define (read-module-definition path . args) (let (
    (module-def-path (format #f "~a/moduledef.scm" path))
    (module-def '())
    (name "")
    (version "")
    (supported-platforms '())
    (is-platform-supported #f)
    (dependency-path '())
    (max-dependency-depth 20)
    (dependencies '())
    (source-directories '())
    (req-modmngr-version "")
    (objects-to-inject '())
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Dependency path parameter passed.
    (set! dependency-path (append dependency-path (list-ref args 0)))
  ))

  ; Check if the module definition file exists.
  (if (not (file-exists? module-def-path))
    (error (format #f "~a ~a"
      "Missing module definition file (\"moduledef.scm\") in current"
      "module path!") path)
  )

  ; Load the module definition and read it.
  ; Reset module definition procedure to detect if there is one defined
  ; by the module to load.
  (define (define-module) #f)
  (load module-def-path)
  (set! module-def (define-module path))

  ; Check if successfully read the module definition.
  (if (eqv? module-def #f)
    (error "Failed to load the module definition!" module-def-path)
  )

  ; Get the module version and name.
  (set! name (string-downcase (strip-directory path)))
  (set! version (map-get-value module-def 'version))

  (if (eqv? version 'key-not-found)
    (error (format #f "~a \"~a\""
      "No version provided by the module definition! Module:" name) module-def)
  )

  ; Check if the requested module manager version range of the module to read
  ; matches with the current module manager version.
  (set! req-modmngr-version (map-get-value module-def 'module-manager-version))

  (if (eqv? req-modmngr-version 'key-not-found)
    (error (format #f "~a \"~a\""
      "No module-manager-version provided by the module definition! Module:"
      name
    ) module-def)
  )

  (if (not (is-version-range-matching?
    module.fscm-module-manager.version req-modmngr-version))
    (error (format #f "~a \"~a\", ~a \"~a\" \"~a\""
      "The current module manager version"
      module.fscm-module-manager.version
      "does not match with the requested version range"
      req-modmngr-version
      "of the module: "
      name
    ) module-def)
  )

  ; Check if the module is already known and only continue if the module is not
  ; known yet. If the module is already known skip and return.
  (if (not (is-known-module? name version)) (begin
    ; Get the supported platforms list of the module.
    (set! supported-platforms
      (map-get-value module-def 'supported-platforms)
    )

    (if (eqv? supported-platforms 'key-not-found)
      (error (format #f "~a Module \"~a\" at version \"~a\"."
        "No supported platforms list provided by the module definition!"
        name version) module-def)
    )

    ; Check if the module supports the current platform.
    (if unix? (begin
      ; Current platform is a unix platform.
      ; Loop through supported platforms of the module.
      (set! is-platform-supported (let next-platform (
          (plat-list supported-platforms)
        )
        (cond
          ; Did not found linux platform in supported platforms list.
          ((null? plat-list) #f)
          ; Found supported platform linux.
          ((eqv? 'linux (car plat-list)) #t)
          ; Did not found platform, therefore call next platform.
          (else (next-platform (cdr plat-list)))
        )
      ))
    ) (begin ; else
      ; Current platform is not a unix platform, assume windows platform.
      ; Loop through supported platforms of the module.
      (set! is-platform-supported (let next-platform (
          (plat-list supported-platforms)
        )
        (cond
          ; Did not found linux platform in supported platforms list.
          ((null? plat-list) #f)
          ; Found supported platform linux.
          ((eqv? 'windows (car plat-list)) #t)
          ; Did not found platform, therefore call next platform.
          (else (next-platform (cdr plat-list)))
        )
      ))
    ))

    ; Only continue if the module is supported by the current platform, if not
    ; print a warning, skip and return.
    (if is-platform-supported (begin
      ; Read dependent modules before adding this module to the modules list.
      (set! dependencies (map-get-value module-def 'dependencies))
      (if (not (eqv? dependencies 'key-not-found)) (begin
        ; Update dependency path and check max dependency depth.
        ;(append-to-list dependency-path (list name version))
        (set! dependency-path
          (append dependency-path (list (list name version)))
        )
        (if (<= (length dependency-path) max-dependency-depth) (begin
          ; Max dependency depth not reached yet, therefore read dependent
          ; modules.
          (read-dependent-modules path dependencies dependency-path)
        ) (begin ; else
          (display (format #f "~a ~a, ~a \"~a\" ~a \"~a\".\n~a ~a\n~a ~a\n"
            "Reached maximum dependency depth of" max-dependency-depth
            "therefore will not read more dependent modules of the module"
            (list-ref (list-ref dependency-path 0) 0) ; module name
            "at version"
            (list-ref (list-ref dependency-path 0) 1) ; module version
            "Execution will continue, but there may be unexpected behaviour or"
            "errors." "Dependency path:" dependency-path
          ))
        ))
      ) (begin ; else
        ; No dependencies for this module, therefore set to empty list.
        (set! dependencies '())
      ))

      ; Check if source directories exist.
      (set! source-directories (map-get-value module-def 'src-dirs))
      (if (not (eqv? source-directories 'key-not-found)) (begin
        (let next-src-dir ((src-dirs source-directories))
          (cond
            ; Reached end of list.
            ((null? src-dirs) #t)
            ; Check if current source directory exists.
            ((not (file-directory? (format #f "~a/~a" path (car src-dirs))))
              ; Source directory does not exist.
              (error "The source directory does not exist!"
                (format #f "~a/~a" path (car src-dirs)))
            )
            ; Else go to the next list entry.
            (else (next-src-dir (cdr src-dirs)))
          )
        )
      ) (begin ; else
        ; No additional source directories, therefore set to empty list.
        (set! source-directories '())
      ))

      ; Check if there is a list of objects to inject into the module
      ; environment.
      (set! objects-to-inject (map-get-value module-def 'objects-to-inject))
      (if (eqv? objects-to-inject 'key-not-found) (begin
        ; No objects to inject, therefore set to empty list.
        (set! objects-to-inject '())
      ))

      ; Add the module definition to the modules list.
      (append-to-list modules (list
        ; The name of the module.
        (list 'name name)

        ; The version of the module.
        (list 'version version)

        ; The platforms this module supports.
        (list 'supported-platforms supported-platforms)

        ; Other modules this module depends on, will be loaded before this
        ; module is loaded. At this point they should already be resolved. They
        ; will only be saved for the sake of completeness.
        (list 'dependencies dependencies)

        ; Additional source code directories to search for *.scm files when
        ; loading the module.
        (list 'src-dirs source-directories)

        ; A list of objects to inject into the module environment.
        (list 'objects-to-inject objects-to-inject)

        ; True if the module is already loaded.
        (list 'loaded #f)

        ; The module path.
        (list 'path path)

        ; The public procedures of the module to export. Initialized with an
        ; empty list, will be defined when the module is loaded the first time.
        (list 'exports '())
      ))
    ) (begin ; else
      (display (format #f "~a \"~a\" ~a \"~a\" ~a ~a ~a"
        "The module" name "at the version" version "does not support the"
        "current platform! Execution will continue, but there may be unexpected"
        "behaviour or errors.\n"
      ))
    ))
  ))
))

;*******************************************************************************
; Refreshes the modules list, which means: Loop through all module paths, read
; the module definition and save the settings. The list will only be refreshed
; if the module paths have changed, specifically if the flag
; "should-refresh-modules-list" is set to true. This is the case if a
; path was added by the "add-module-paths" procedure and there was no refresh
; after that.
(define (refresh-modules-list)
  ; Check if modules should be read.
  (if should-refresh-modules-list (begin
    ; Loop through module paths and read their definition.
    (let read-module ((paths module-paths))
      ; Check if reached end of module paths list. If so, return true.
      (if (null? paths) (begin
        ; Reached end of module paths list, therefore return true.
        #t
      ) (begin ; else
        ; Read the module definition and save it to the modules list.
        (read-module-definition (car paths))
        ; Go to the next module path in the list.
        (read-module (cdr paths))
      ))
    )

    ; Modules list refreshed, therefore set the "should-refresh-modules-list"
    ; flag to false.
    (if should-refresh-modules-list
      (set! should-refresh-modules-list #f)
    )
  ))
)

;*******************************************************************************
; Adds module paths to the module manager. Every module path must be an absolute
; path to the parent folder of the module, which contains the module definition
; file, which must have exactly the name "moduledef.scm". See the template
; "moduledef.scm.template" for how to write the module definition file.
;
; Parameters:
;   paths-list - A list of module paths to add.
(define (add-module-paths paths-list) (let (
    (path-is-new #f)
  )
  ; Check argument.
  (if (not (pair? paths-list))
    (error "Invalid type of argument \"paths-list\", must be of type list!"
      paths-list)
  )

  ; Loop through paths to add.
  (for-each (lambda (path)
    ; Loop through module paths list to check if the path already exists.
    (set! path-is-new (let check-next-path ((mod-paths module-paths))
      (cond
        ; Check if reached end of module paths list. If so, return true.
        ((null? mod-paths) #t)
        ; Check if first list element of current module paths list is the path
        ; to add. If so, return false.
        ((equal? path (car mod-paths)) #f)
        ; Current first list element is not equal to path and did not reached
        ; the end of the list, therefore call to next recursion with the paths
        ; list without the first element (cdr command).
        (else (check-next-path (cdr mod-paths)))
      )
    ))

    ; If the path is not already in the paths list and exists, then add it to
    ; the module paths list.
    (if (not (file-directory? path)) (begin
      (error "The module path does not exist!" path)
    ) (if path-is-new (begin ; elseif
      (append-to-list module-paths path)

      (if (not should-refresh-modules-list)
        (set! should-refresh-modules-list #t)
      )
    )))
  ) paths-list)
))

;*******************************************************************************
; Passes all parameters to the import procedure. This procedure is used to
; intercept calls to the import procedure. In an module environment the call to
; import is intercepted by this procedure by assigning it to the 'import symbol
; in the module environment of the module this import was called from. It
; ignores an optional passed environment to the import call and replaces it with
; the passed module environment.
;
; Parameters:
;   parent-module-name - The name of the parent module.
;   module-env - The module environment passed to the import procedure.
;   import-name - Will be passed to the import procedure, see the import
;     procedure for the parameter description.
;   module-name - Will be passed to the import procedure, see the import
;     procedure for the parameter description.
;   version-range - [optional, default: ""] Will be passed to the import
;     procedure, see the import procedure for the parameter description.
;   force-load - [optional, default: #f] Will be passed to the import
;     procedure, see the import procedure for the parameter description.
;   parent-environment - [optional, default: user-initial-environment] If 
;   passed, will be ignored and replaced by the module-env parameter in the 
;   import call.
(define (import-interceptor parent-module-name module-env import-name
  module-name . args) (let (
    (version-range "")
    (force-load #f)
  )
  ; Parse optional arguments.
  (if (> (length args) 0)
    ; Version range parameter passed.
    (set! version-range (list-ref args 0))
  )

  (if (> (length args) 1)
    ; Force load parameter passed.
    (set! force-load (list-ref args 1))
  )

  ; Check if the module to import is not the same as the parent module.
  (if (string=? parent-module-name module-name)
    (error (string-append
      "Circular dependency, can't load the \"" module-name "\" module inside "
      "the \"" parent-module-name "\" module!"
    ) module-name)
  )

  ; Call the import procedure.
  (import import-name module-name version-range force-load module-env)
))

;*******************************************************************************
; Imports a module. The module will only be loaded once. If you want to force
; a reload, you have to pass true to the force-load parameter.
; > NOTE: The path to the module has to be added by the procedure
; > "add-module-paths" before you can import the module.
;
; Parameters:
;   import-name - The import name of the module. Each exported procedure of the
;     module will be loaded into the environment following the syntax:
;     `<import-name>/<procedure-name>`, e.g.:  
;     + import-name = utils  
;     + procedure-name = get-unique-file-path  
;     + => `utils/get-unique-file-path`  
;   module-name - The name of the module to load.
;   version-range - [optional, default: ""] The version of the module to import.
;     If the parameter is given as a version range, than the newest module which
;     satisfies the version-range will be imported. Examples:  
;     + `"2.3.4"` - specific version, only the module with exactly this version
;       will be imported.
;     + `">=1.2.2"` - The newest module with a version bigger or equal to
;       `"1.2.2"` will be imported. Other possible operators are: `">"`, `"<"`,
;       `"<="`, `"="` If no version operator is given, `"="` is assumed.
;     + `"2.3.*"` - The newest module which majo version is 2 and minor version
;       is 3 will be imported. The patch version will be ignored, because
;       of the wildcard `"*"` (instead of `"*"`, `"x"` or `"X"` can also be 
;       used). The wildcard can also be used for the minor and/or major version.
;   force-load - [optional, default: #f] By default a module will only be loaded
;     once, but if this parameter is true, than the module will be loaded,
;     regardless if it is loaded already.
;   parent-environment - [optional, default: user-initial-environment] The
;     environment from which the module environment should inherit.
(define (import import-name module-name . args) (let (
    (version-range "")
    (force-load #f)
    (parent-env '())
  )
  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Version range parameter passed.
    (set! version-range (list-ref args 0))
    (if (not (string? version-range))
      (error "Invalid type of argument \"version-range\", must be a string!"
        version-range)
    )
  ))

  (if (> (length args) 1) (begin
    ; Force load parameter passed.
    (set! force-load (list-ref args 1))
    (if (not (boolean? force-load))
      (error "Invalid type of argument \"force-load\", must be of type boolean!"
        force-load)
    )
  ))

  (if (> (length args) 2) (begin
    ; Parent environment parameter passed.
    (set! parent-env (list-ref args 2))
  ) (begin ; else
    (set! parent-env user-initial-environment)
  ))

  ; Check and convert import-name parameter.
  (if (string? import-name) (set! import-name (string->symbol import-name)))
  (if (not (symbol? import-name))
    (error (string-append
      "Invalid type of argument \"import-name\", must either be string or "
      "symbol!") import-name)
  )

  ; Check and convert module-name parameter.
  (if (symbol? module-name) (set! module-name (symbol->string module-name)))
  (if (not (string? module-name))
    (error (string-append
      "Invalid type of argument \"module-name\", must either be string or "
      "symbol!") module-name)
  )

  ; Refresh the modules list if needed.
  (refresh-modules-list)

  ; Load the module if not already loaded. If force-load is true, load module
  ; regardless if it is already loaded.
  (load-module import-name module-name version-range force-load parent-env)
))

;*******************************************************************************
; Prints some information about the known modules.
(define (print-modules-list) (let (
    (deps '())
    (oti '())
    (env '())
    (exports '())
  )
  (for-each (lambda (module)
    (display (format #f "~a/~a:\n"
      (map-get-value module 'name) (map-get-value module 'version)))
    (display (format #f "  -> loaded: ~a\n" (map-get-value module 'loaded)))
    (display (format #f "  -> path: ~a\n" (map-get-value module 'path)))
    (display (format #f "  -> supported-platforms: ~a\n"
      (map-get-value module 'supported-platforms)))
    (display (format #f "  -> src-dirs: ~a\n" (map-get-value module 'src-dirs)))

    (set! deps (map-get-value module 'dependencies))
    (if (or (null? deps) (null? (car deps))) (begin
      (display "  -> dependencies: no dependencies\n")
    ) (begin ; else
      (display "  -> dependencies:\n")
      (for-each (lambda (dep) (display (format #f "    + ~a\n" dep))) deps)
    ))

    (set! exports (map-get-value module 'exports))
    (display "  -> exports:\n")
    (for-each (lambda (export)
      (display (format #f "    + ~a\n" (car export)))
    ) exports)

    (set! oti (map-get-value module 'objects-to-inject))
    (if (or (null? oti) (null? (car oti))) (begin
      (display "  -> objects to inject: no objects to inject\n")
    ) (begin ; else
      (if (> (length oti) 50) (begin
        (display "  -> objects to inject: to much to print\n")
      ) (begin ; else
        (display "  -> objects to inject:\n")
        (for-each (lambda (o) (display (format #f "    + ~a\n" (car o)))) oti)
      ))
    ))
  ) modules)
))

;*******************************************************************************
; Export public module manager procedures.
(export (list
  (list 'add-module-paths add-module-paths)
  (list 'import import)
  (list 'print-modules-list print-modules-list)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
