;*******************************************************************************
; Implements some helper procedures for the module manager.

;*******************************************************************************
; Appends a value to the list.
;
; Parameters:
;   lst - The list to which the value to append.
;   value - The value to append.
(define (append-to-list lst value)
  (cond
    ; Return false if list is empty
    ((null? lst) #f)
    ; If first element is '() (= null, empty element) then set car with value.
    ((and (= (length lst) 1) (null? (car lst))) (set-car! lst value))
    ; Otherwise append to list.
    (else (set-cdr! (list-tail lst (- (length lst) 1)) (list value)))
  )
)

;*******************************************************************************
; Retrieves the value of a key in a key value map list.
;
; Parameters:
;   key-value-list - The key value map list.
;   key - The key from which to read the value.
;
; Returns: The value of the key.
(define (map-get-value key-value-list key)
  (let next-entry (
      (lst key-value-list)
      (curr-entry '())
    )
    ; Check if reached end of list and return false if so.
    (if (null? lst) (begin
      ; Key not found.
      'key-not-found
    ) (begin ; else
      (set! curr-entry (car lst))
      (cond
        ; Check if current entry has the requested key. If so return it`s value.
        ((eqv? key (car curr-entry)) (list-ref curr-entry 1))
        ; Current entry has not the key, therefore go to the next entry.
        (else (next-entry (cdr lst) '()))
      )
    ))
  )
)

;*******************************************************************************
; Sets the value of a key in a key value map list.
;
; Parameters:
;   key-value-list - The key value map list.
;   key - The key in the map, where to write the value.
;   value - The value to write.
(define (map-set-value key-value-list key value) (let (
    (current-value (map-get-value key-value-list key))
  )
  (if (eqv? current-value 'key-not-found) (begin
    ; Key does currently not exist, therefore create it.
    (append-to-list key-value-list (list key value))
  ) (begin ; else
    ; Key does exist, therefore check if the value has changed and if so update
    ; the value.
    (if (not (eqv? current-value value))
      ; Value of key has changed, therefore update it.
      (let next-entry ((lst key-value-list))
        (cond
          ; This should not occur, because checked before that the key exists.
          ((null? lst) (error (format #f
            "Failed to set key-value-list value! key = ~a, value = ~a"
            key value) key-value-list)
          )
          ; Check if found key and if so set it`s value.
          ((eqv? key (car (car lst))) (set-car! lst (list key value)))
          ; Current entry has not the key, therefore go to the next one.
          (else (next-entry (cdr lst)))
        )
      )
    )
  ))
))

;*******************************************************************************
; Reads the content of the given file (path) and converts it to an object list.
; Objects are interpreted as strings seperated by whitespace.
;
; Parameters:
;   inputfile - The file from which to read.
;
; Returns: A list of read objects.
(define (file-content-to-list inputfile) (let (
    (filePort (open-input-file inputfile))
  )
  ; Use named let to loop through objects in file and build a list from it.
  (let readObject ((object (read filePort)))
    (if (eof-object? object) (begin
        (close-input-port filePort)
        ; Return empty list if no objects in the file, or empty list inidcating
        ; the last element of the objects list.
        '()
      ) (begin ;else
        ; Add the read object to the list and read the next one by recursively
        ; call the readObject procedure (= named let).
        (cons object (readObject (read filePort)))
      )
    )
  )
))

;*******************************************************************************
; Retrieves all source files of a directories. Source files are all files with
; the file extension `".scm"`. The developer has to guarantee that all source
; in the directory are valid fluent scheme files.
;
; Parameters:
;   directory - The directory in which to search for source files.
;
; Returns: A list of source file names in the directory. If no files were found
;   an empty list is returned.
(define (get-source-files-of-directory directory) (let (
    (get-files-cmd "ls -p *.scm | grep -v /")
    (tmp-file-name "tmp.out")
    (tmp-file-path "")
    (source-files-list '())
    (backup-directory "")
  )
  ; Initialize variables.
  (set! tmp-file-path (format #f "~a/~a" directory tmp-file-name))

  ; Save the current directory path.
  (system (format #f "pwd > \"~a\"" tmp-file-name))
  (set! backup-directory (list-ref (file-content-to-list tmp-file-name) 0))
  (remove-file tmp-file-name)

  ; Build the system command to retrieve the files. The command will get all
  ; source files (pattern = "*.scm") of the directory and pipe the output to a
  ; temporary file.
  (system (format #f "cd \"~a\" && ~a > \"~a\" && cd \"~a\""
    directory ; cd to the directory of which to get the source files
    get-files-cmd ; The command which retrieves the file names.
    tmp-file-name ; The file where the output will be piped to.
    backup-directory ; cd back to the former directory
  ))

  ; Now read the file and convert the file content to an object list with the
  ; source file names.
  (set! source-files-list (file-content-to-list tmp-file-path))
  (remove-file tmp-file-path)

  ; Return the source files list.
  source-files-list
))

;*******************************************************************************
; Loads all source files in the given directory.
;
; Parameters:
;   directory - The directory of which to load the source files.
;   environment - The environment where to load the files.
(define (load-source-files-of-directory directory environment) (let (
    (source-files-list (get-source-files-of-directory directory))
  )
  ; Loop through the source files list of the source directory.
  (for-each (lambda (source-file)
    ; Only load the file if it`s not the module definition file.
    (if (not (string=? (format #f "~a" source-file) "moduledef.scm")) (begin
      ; Load the source file.
      (load (format #f "~a/~a" directory source-file) environment)
    ))
  ) source-files-list)
))

;*******************************************************************************
; Creates a new module environment.
;
; Parameters:
;   module-import-path - The import path of the module.
;   import-name - The import name of the module for which to create the
;     environment.
;   parent-environments - The parent environments of the module environment to
;     create (list).
;   name - The module name.
;   objects-to-inject - The objects to inject to the new environment as name
;     value pair list. The object names must be symbols. e.g.:
;     ```scheme  
;     (list
;       (list name1 value1)
;       (list name2 value2)
;       ...
;     )
;     ```
;   export-proc - The export procedure for the module environment, must be a
;     procedure. The procedure will be added to the environment with the name
;     `"export"` and must accept one parameter:  
;     + `exports` - A name value pair list with the procedures of the module to
;       export. The name must be a symbol and the value a procedure. Normally
;       the name should be the same as the procedure name, but this is not
;       mandatory. e.g.:  
;
;       ```scheme
;       (list
;         (list 'proc1 proc1)
;         (list 'proc2 proc2)
;         ...
;       )
;       ```
;
; Returns: The newly created module environment.
(define (create-module-enviroment module-import-path import-name
  parent-environments name objects-to-inject export-proc) (let (
    (oti-names '()) ; objects to inject names
    (oti-values '()) ; objects to inject values
    (module-environment '())
    (i 0)
  )
  ; Get names and values of objects to inject.
  (for-each (lambda (obj)
    (set! oti-names (append oti-names (list (car obj))))
    (set! oti-values (append oti-values (list (cadr obj))))
  ) objects-to-inject)

  ; Create the module environment.
  (set! module-environment (append (list (append
    ; The environment object names.
    (list (append
      (list
        'import ; The name for the import procedure.
        'export ; The name for the export procedure.
        'environment-for-module ; Name of the module this environment is for.
        'module-import-path ; Name of the module import path variable.
      )
      oti-names ; Names of the objects to inject.
    ))
    ; The environment object values.
    (append
      (list
        (lambda (import-name module-name . args) ; import interceptor
          ; Intercept the call to the import procedure to inject the parent
          ; environment to which this module should be loaded.
          (apply import-interceptor (append
            (list name module-environment import-name module-name) args))
        )
        export-proc ; export procedure
        name ; Name of the module this environment is for.
        (if (null? module-import-path) ; The module import path.
          (list import-name)
          (append module-import-path (list import-name))
        )
      ) ; The export procedure.
      oti-values ; Values of the objects to inject.
    )
  )) parent-environments))

  ; Return the created module environment.
  module-environment
))

;*******************************************************************************
; Sets the loaded flag of a module to true.
;
; Parameters:
;   name - The module name.
;   version - The specific version of the module. Not a version range!
(define (set-module-loaded name version)
  (let next-entry ((modules modules))
    (cond
      ; Reached end of modules list.
      ((null? modules) #f)
      ; Check if the current module is the one to set loaded.
      ((and
        (string=? name (map-get-value (car modules) 'name))
        (string=? version (map-get-value (car modules) 'version))
      ) (let (
          (module (car modules))
        )
        ; Found module, so set it loaded.
        (map-set-value module 'loaded #t)
        (set-car! modules module)

        ; Return true for success.
        #t
      ))
      ; Module not found, therefore go to the next one.
      (else (next-entry (cdr modules)))
    )
  )
)

;*******************************************************************************
; Retrieves the settings of a module (defined by the parameters name and
; optional version), which is an entry of the modules list.
;
; Parameters:
;   name - The module name.
;   version-range - [optional, default: ""] The version range to match against.
;
; Returns: The settings of a module.
(define (get-module-settings name . args) (let (
    (version-range "")
    (module-settings '())
    (curr-version "")
    (is-version-matching #f)
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; Version range parameter passed.
    (set! version-range (list-ref args 0))
  ))

  ; Loop through known modules to find the module settings of the requested
  ; module.
  (for-each (lambda (m)
    (if (string=? name (map-get-value m 'name)) (begin
      (set! curr-version (map-get-value m 'version))

      ; Check if version range is matching.
      (set! is-version-matching (cond
        ; True when no version range was passed.
        ((string=? version-range "") #t)
        ; When a version range was given, check it and return true when matching.
        ((is-version-range-matching? curr-version version-range) #t)
        ; Return false when version range does not match.
        (else #f)
      ))

      ; Only continue if found a match for the version range.
      (if is-version-matching (begin
        ; Check if the current match module is newer than the previous found
        ; module (if there was any).
        (if (not (null? module-settings)) (begin
          (if (is-version-range-matching?
            (map-get-value module-settings 'version)
            (string-append "<" curr-version)
          ) (begin
            ; The version of the current module is newer than the previous one
            ; found, therefore save it (replace the previous one).
            (set! module-settings m)
          ))
        ) (begin ; else
          ; There was no module found before, therefore save the current.
          (set! module-settings m)
        ))
      ))
    ))
  ) modules)

  ; Return the module settings.
  module-settings
))

;*******************************************************************************
; Checks if the module defined by the given name and version-range is known by
; the module manager (is in the modules list).
;
; Parameters:
;   name - The name of the module.
;   version-range - The version range to match against.
;
; Returns: True if the module is known (the version-range matches with a known
;   one), false otherwise.
(define (is-known-module? name version-range) (let (
    (is-modules-list-empty #t)
  )
  ; First check if modules list is empty.
  (set! is-modules-list-empty (and
    (= (length modules) 1)
    (null? (list-ref modules 0))
  ))

  (if is-modules-list-empty (begin
    ; Modules list is empty, therefore return false.
    #f
  ) (begin ; else
    ; Modules list is not empty.
    (let next-module ((modules modules))
      (cond
        ; If reached last element of list, than the module is not known.
        ((null? modules) #f)
        ; Check if found the module and return true if so.
        ((and
          ; Check names.
          (string=? name (map-get-value (car modules) 'name))
          ; Check version-range against version.
          (is-version-range-matching?
            (map-get-value (car modules) 'version)
            version-range
          )
        ) #t)
        ; Go to next module if list is not empty and the module was not found
        ; yet.
        (else (next-module (cdr modules)))
      )
    )
  ))
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
