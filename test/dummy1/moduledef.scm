;###############################################################################
; The module definition, this procedure must have exactly the name defined
; below and will be called by the module manager with one parameter, the
; absolute path to the module.
;
; Parameters:
;   module-path - The absolute path to the module.
;
; Returns: The module definition key value map (list).
(define (define-module module-path) (let ( (module-def '()) )
  (load (format #f "~a/module-version.scm" module-path))
  (set! module-def (list
    ; Module version according to semantic versioning, see: http://semver.org
    (list 'version module.dummy1.version)

    ; List of supported platforms by the module, valid platforms are: linux,
    ; windows
    (list 'supported-platforms (list 'linux))

    ; List of modules this module depends on. A list entry is a list with three
    ; entries (the second and third are optional):
    ;   (1) The module name, which is the name of the directory containing the
    ;       module definitin file ("moduledef.scm").
    ;   (2) The version range for the module which defines what module versions
    ;       are valid. If ommited, than the newest available version of the
    ;       module will be used.
    ;   (3) The path to the module, relative to this file. If omitted, than the
    ;       path to the module must be added otherwise (or another definition
    ;       file has already or will be adding the path to the module).
    (list 'dependencies (list '())) ; no dependencies

    ; A list of source directories, relative to this file. By default the module
    ; manager will only search for *.scm files in the module directory (which
    ; must be the directory of this file). If there are source (*.scm) files in
    ; subdirectories, you have to add them.
    ; NOTE: The module manager will not serach for source files above the module
    ; directory, even if you add them to the source directories!
    (list 'src-dirs (list "src"))
  ))

  ; Return the module definition.
  module-def
))
