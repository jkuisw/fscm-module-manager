(define (hello-world)
  (display (format #f "Module \"~a (~a)\" says: Hello world!\n"
    "dummy3" module.dummy3.version))
)

(hello-world)
