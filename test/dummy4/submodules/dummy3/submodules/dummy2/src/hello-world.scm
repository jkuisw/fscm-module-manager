(define (hello-world)
  (display (format #f "Module \"~a (~a)\" says: Hello world!\n"
    "dummy2" module.dummy2.version))
)

(hello-world)
