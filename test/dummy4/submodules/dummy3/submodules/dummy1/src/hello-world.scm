(define (hello-world)
  (display (format #f "Module \"~a (~a)\" says: Hello world!\n"
    "dummy1" module.dummy1.version))
)

(hello-world)
