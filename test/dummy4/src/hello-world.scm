(define (hello-world)
  (display (format #f "Module \"~a (~a)\" says: Hello world!\n"
    "dummy4" module.dummy4.version))
)

(hello-world)
