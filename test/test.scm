; Start the test script (fluent) in the repo folder "test", so that pwd returns
; the right absolute path.
(define modmngr.test.basedir (let (
    (filePort "")
    (result "")
  )
  (system "pwd > \"tmp.out\"")
  (set! filePort (open-input-file "tmp.out"))
  (set! result (read-line filePort))
  (close-input-port filePort)
  result
))

(display modmngr.test.basedir)
(newline)

(define (lmm)
  (define env user-initial-environment)
  (load (format #f "~a/../setup-module-manager.scm" modmngr.test.basedir) env)
  (init-module-manager (format #f "~a/.." modmngr.test.basedir) env)
)

(define (lmmt)
  (lmm)
  (newline)
  (display "add-module-paths:\n")
  (add-module-paths (list (format #f "~a/dummy1" modmngr.test.basedir)))
  (add-module-paths (list (format #f "~a/dummy2" modmngr.test.basedir)))
  (add-module-paths (list (format #f "~a/dummy3" modmngr.test.basedir)))
  (add-module-paths (list (format #f "~a/dummy4" modmngr.test.basedir)))
  (display modmngr.module-paths)
  (newline)

  (display "modmngr.refresh-modules-list:\n")
  (modmngr.refresh-modules-list)
  (display "modmngr.modules list:\n")
  (for-each (lambda (module)
    (display module) (newline)
  ) modmngr.modules)
)

; Expected results:
;((name dummy1) (version 0.5.1) (supported-platforms (linux)) (dependencies (())) (src-dirs (src)) (loaded #f) (path /home/georg/Programmieren/jkuisw/fscm-module-manager/test/dummy1))
;((name dummy2) (version 0.2.1) (supported-platforms (linux)) (dependencies (())) (src-dirs (src)) (loaded #f) (path /home/georg/Programmieren/jkuisw/fscm-module-manager/test/dummy2))
;((name dummy2) (version 0.0.5) (supported-platforms (linux)) (dependencies (())) (src-dirs (src)) (loaded #f) (path /home/georg/Programmieren/jkuisw/fscm-module-manager/test/dummy3/submodules/dummy2))
;((name dummy3) (version 2.7.6) (supported-platforms (linux)) (dependencies ((dummy1 0.*.* submodules/dummy1) (dummy2 0.0.5 submodules/dummy2))) (src-dirs (src)) (loaded #f) (path /home/georg/Programmieren/jkuisw/fscm-module-manager/test/dummy3))
;((name dummy4) (version 1.5.3) (supported-platforms (linux)) (dependencies ((dummy1 >=0.4.0 submodules/dummy1) (dummy3 2.*.* submodules/dummy3))) (src-dirs (src)) (loaded #f) (path /home/georg/Programmieren/jkuisw/fscm-module-manager/test/dummy4))
