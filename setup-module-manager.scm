;*******************************************************************************
; This file must be loaded to use the module manager. It configures the module
; manager and provides an initialization procedure, which must be called with
; the absolute path to the module manager repository. So to use the module
; manager you have to execute the two following commands:
;   (load "/absolute/repository/path/setup-module-manager.scm")
;   (init-module-manager "/absolute/repository/path")
; Where "/absolute/repository/path" is the absolute path to the module manager
; repository.

;*******************************************************************************
; Module global variables.
(define modmngr-environment)

;*******************************************************************************
; Initializes the module manager.
;
; Parameters:
;   path - The absolute path to the module manager directory.
(define (init-module-manager path) (let (
    (oti-names '()) ; objects to inject names
    (oti-values '()) ; objects to inject values
    (objects-to-inject '())
  )
  ; Check if the given path exists.
  (if (not (file-directory? path)) (error "The path does not exists!" path))

  ; Check if the path is valid.
  (if (not (file-exists? (format #f "~a/setup-module-manager.scm" path)))
    (error "The given path is not the path to the module manager repository!"
      path)
  )

  ; Create the module manager environment and inject needed objects.
  (for-each (lambda (obj)
    (set! oti-names (append oti-names (list (car obj))))
    (set! oti-values (append oti-values (list (cadr obj))))
  ) objects-to-inject)

  (set! modmngr-environment (list (append
    ; Module manager environment object names.
    (list (append (list
      'environment-for-module ; Name of the module this environment is for.
      'repository-path ; Name of the repository path variable.
      'export ; Name of the module manager export procedure.
    ) oti-names)) ; Names of objects to inject.
    ; Module manager environment object values.
    (append (list
      "fscm-module-manager"
      path
      ; Internal export procedure for the module manager's public procedures.
      (lambda (exports)
        (for-each (lambda (export-pair)
          ; Export the procedure to the global environment, which is the
          ; "user-initial-environment" environment.
          (eval (list 'define
            (string->symbol (car export-pair)) ; Name of the procedure to export.
            (cadr export-pair) ; The procedure to export.
          ) user-initial-environment)
        ) exports)
      )
    ) oti-values) ; Values of objects to inject.
  ) (car user-initial-environment)))

  ; Load all module manager source files.
  (load (format #f "~a/~a" path "module-version.scm") modmngr-environment)
  (load (format #f "~a/src/~a" path "helpers.scm") modmngr-environment)
  (load (format #f "~a/src/~a" path "version-parsing.scm") modmngr-environment)
  (load (format #f "~a/src/~a" path "import-tree.scm") modmngr-environment)
  (load (format #f "~a/src/~a" path "module-manager.scm") modmngr-environment)
))
