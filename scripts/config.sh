#!/bin/bash

################################################################################
# Fluent scheme module repository scritps configuration
# ASSUMPTION: The "isw-git-tools" was added as git submodule.
################################################################################

################################################################################
# BEGIN OF CONFIGURATION SECTION
# CONFIGURE THE TEMPLATE HERE
################################################################################

# The path to the "isw-git-tools" submodule directory relative from the location
# of this config file, e.g.:
# "../submodules/isw-git-tools"
modulePath="../submodules/isw-git-tools"

# The path to the directory of the module repository root, relative from the
# location of this config file.
# e.g.: ".."
# shellcheck disable=SC2034
repositoryDirectory=".."

# The git branch which is used for productive code (not for development)
# normally this is the master branch.
# shellcheck disable=SC2034
productionBranch="master"

# The path to the directory where the source code documentation should be
# located, relative from the location of this config file.
# e.g.: "../doc"
# shellcheck disable=SC2034
documentationDirectory="../doc"

# The path to the directory where the source code is located (root directory),
# relative from the location of this config file.
# e.g.: "../src"
# shellcheck disable=SC2034
sourceDirectory="../src"

################################################################################
# END OF CONFIGURATION SECTION
# DO NOT CHANGE CODE BELOW THIS LINE!
################################################################################

# Get absolute path to script directory
scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")

# Resolve realtive paths.
modulePath=$(readlink -f "$scriptDir/$modulePath")
repositoryDirectory=$(readlink -f "$scriptDir/$repositoryDirectory")
documentationDirectory=$(readlink -f "$scriptDir/$documentationDirectory")
sourceDirectory=$(readlink -f "$scriptDir/$sourceDirectory")

# Resolve path to repo scripts for a fluent scheme module repository.
# shellcheck disable=SC2034
scriptPath=$(readlink -f "$modulePath/repo-scripts/fluent-scheme/scripts")
