Contributing to the Fluent Scheme Module Manager
================================================

It is highly appreciated if you want to contribute, but please read the
following guide carefully before you do so.

Directory Structure
-------------------
The following code snippet shows the directory structure of the repository.

```
fscm-module-manager
|--+ doc
|--- docker
|  +--- fedora26-git
|     +--- Dockerfile
|
|--+ resources
|--+ scripts
|--+ src
|--+ submodules
|--+ test
|--- .gitignore
|--- .gitlab-ci.yml
|--- .gitmodules
|--- CONTRIBUTING.md
|--- LICENSE
|--- module-version.scm
|--- moduledef.scm.template
|--- README.md
+--- setup-module-manager.scm
```

Explanation for the directories:
* `doc` - Location for the generated API documentation, see the
  [fluent scheme language support][isw-git-tools-fluent-scheme].
* `docker` - Location for the Dockerfiles, see the
  [fluent scheme language support][isw-git-tools-fluent-scheme].
* `resources` - Some images, icons and so on needed for documentation.
* `scripts` - For the script wrappers and the configuration of them, see the
  [fluent scheme language support][isw-git-tools-fluent-scheme].
* `src` - Where the source files of this module are located (the actual
  implementation of the module).
* `submodules` - The git submodules this module depends on. This module depends
  only on the [isw git tools repository][isw-git-tools-repo].
* `test` - Some test data to test the module manager implementation.

Explanation for the files:
* `.gitignore` - Defines file and directory patterns for files and/or
  directories that should be ignored by git. See the
  [git ignore documentation][git-ignore] for details. 
* `.gitlab-ci.yml` - The [GitLab][gitlab] CI configuration, see the
  [fluent scheme language support][isw-git-tools-fluent-scheme].
* `.gitmodules` - The git submodules configuration, see the
  [git submodules documentation][git-submodules] for details.
* `module-version.scm` - Stores the module version in a scheme source file.
  Generated by the create version script. See the
  [fluent scheme language support][isw-git-tools-fluent-scheme] for details.
* `moduledef.scm.template` - Template for a module definition file.
* `setup-module-manager.scm` - The setup script for the module manager. It is
  the first script that must be loaded to use the module manager and fluent
  scheme modules. It provides an initialization procedure, which has to be
  called after loading the setup script. The initialization procedure creates
  then the module manager environment and loads the source files of the module
  manager. Therefore if you implement a new source for the module manager, you
  have to load it at the end of that procedure.

API Documentation Generator Customization
-----------------------------------------
The module manager exports it's public procedures into the global scheme
environment (no import name). Therefore the procedures list description of the
generated API documentation has to be customized. This is done by the file
`exported-procs-list-description.md` in the `src` folder.

The Source Files
----------------
### The `helpers.scm` file
To avoid circular dependencies, the module manager must not depend on any other
fluent scheme modules. Therefore some helper procedures are needed which would
normally be implemented in other modules. Furthermore to keep the source files
short and clean some tasks are re-factored in procedures and implemented in the
`helper.scm` source file.

### The `import-tree.scm` file
This part of the module implements procedures for interacting with the import
tree. The import tree tracks the relations between all imported modules, their
import names and their "child" modules. With the import tree the module manager
knows which module is imported with which import name and to which module
environment. 

### The `module-manager.scm` file
Implements the core functionality of the module manager, which is:
* Adding module paths (register modules) to find fluent scheme modules.
* Search for modules in the added module paths.
* Read the module definition file of the modules.
* Load modules when requested and needed.
* Import modules to source files.
* Print information about the known and loaded modules.

### The `version-parsing.scm` file
This file implements parsing and matching of [semantic versioning][semver-spec]
strings. The [semantic versioning][semver-spec] 2.0.0 specification is fully
implemented. For version string matching the
[npmjs version ranges][npmjs-version-ranges] are used. Except for the
"Advanced Range Syntax", all version-range features are supported and
implemented.

Development Workflow
--------------------
The production branch for the `fscm-module-manager` repository is the `master`
branch, which is also a [protected branch][gitlab-protected-branches]. Only
users with at least the `Master` permission level can push or merge to this
branch (see the [GitLab permissions model][gitlab-permissions-model] for further
information). For the development of this repository the
[Development Workflow][isw-git-tools-dev-workflow] should be used. Please read
this guide and try to follow it.

Versioning
----------
You may only create versions of this repository that follow the
[semantic versioning specification][semver-spec]. To create a new version, just
go to the project page on [GitLab][gitlab]. Search for the `Tags` submenu in the
`Repository` menu. Click the `New tag` button and choose for the tag name a
version following the [semantic versioning specification][semver-spec].
Furthermore document in the release notes what has changed to the prior version
(following the [Development Workflow][isw-git-tools-dev-workflow]). Then click
the `Create tag` button.



<!-- links reference -->
[isw-git-tools-repo]: https://gitlab.com/jkuisw/isw-git-tools
[isw-git-tools-dev-workflow]: https://gitlab.com/jkuisw/isw-git-tools/blob/master/guides/dev-workflow.md
[isw-git-tools-fluent-scheme]: https://gitlab.com/jkuisw/isw-git-tools/tree/master/repo-scripts/fluent-scheme

[gitlab]: https://gitlab.com/
[gitlab-permissions-model]: https://docs.gitlab.com/ee/user/permissions.html
[gitlab-protected-branches]: https://docs.gitlab.com/ee/user/project/protected_branches.html#protected-branches
[git-submodules]: https://git-scm.com/book/en/v2/Git-Tools-Submodules
[git-ignore]: https://git-scm.com/docs/gitignore
[semver-spec]: http://semver.org/
[npmjs-version-ranges]: https://docs.npmjs.com/misc/semver
