Fluent Scheme Module Manager
============================

The fluent scheme (fscm) module manager provides a mechanism for grouping 
related fluent scheme source code to modules. This modules can than be loaded
dynamically (as needed) into fluent scheme script files. The module manager is 
responsible for handling different versions of the same module, handling many 
different modules, finding the source files and loading them when needed. 
Obviously the module manager must be setup correctly before a fscm module can 
be used. Furthermore modules need to be registered to the module manager so that 
it knows where to find them. Below the setup and basic usage of the module 
manager is described, if you are only looking for the API documentation, 
[click here][api-doc] (located in the `doc` folder).

If you'd like to contribute, please read the 
[contribution guide](CONTRIBUTING.md) first.

Module Manager Setup
--------------------
Before proceeding, make sure that the GitLab access on your user account is
correctly configured (needs to be done only once per operating system user
account). If not, read the [Setup GitLab Access][setup-gitlab-access-guide]
guide to configure the access properly.

### Get the code
First you need to get the module manager code, therefore choose a location where
you want to store the module manager repository. For example you could create 
the directory `jkuisw` in your home folder and clone all `jkuisw` related 
repositories to this folder. 

> :information_source: All fluent scheme module repositories, including the 
> module manager repository, are assigned to the 
> [gitlab group `jkuisw`][jkuisw-group].

To clone the repository, go to the directory (e.g.: 
`cd /home/<user name>/jkuisw`) and execute the following command:  

```shell
git clone --recurse-submodules git@gitlab.com:jkuisw/fscm-module-manager.git
```

This command will create the folder `fscm-module-manager` and fetch the 
repository from gitlab into that folder. The parameter `--recurse-submodules` is
mandatory, because the module manager has [submodules][git-submodules] and they
are only fetched if this parameter is passed.  

### The `.fluent` file
The next step is to tell fluent that it should load, setup and initialize the 
module manager when it is started. For this we will use the `.fluent` file. This
file is a scheme script that will be executed by fluent every time on startup.
It must be created in your home folder and exactly named `.fluent` (see the
fluent documentation for further infomation). The following command will create
it (no quotes, because with quotes `~` will not be resolved):

```shell
touch ~/.fluent
```

![.fluent file structure][dot-fluent-file-structure]

### Setup and initialization
Now we have our entry point to setup the module manager. For this the module
manager provides a setup script called `setup-module-manager.scm` that must be
loaded. After that the module manager must be initialized with the 
`init-module-manager` command. This command has one parameter, the absolute path
to the module manager repository (this is needed, so that the module can find
all source files). Therefore open the `.fluent` file in an editor and add the
following lines to the beginning of the file:

```scheme
; Create a variable to store the absolute path to the jkuisw directory. You 
; could as well use the entire path below without this variable. But with the 
; variable you avoid mistakes and need to write less code. Adapt the path to 
; your environment accordingly.
(define jkuisw-path "/home/<user name>/jkuisw")

; Setup the module manager.
; Loads and executes the module manager setup script.
(load (string-append jkuisw-path "/fscm-module-manager/setup-module-manager.scm"))
; Initializes the module manager.
(init-module-manager (string-append jkuisw-path "/fscm-module-manager"))
```

### Register modules
After that the module manager is set up and initialized. Furthermore the module
manager needs to know **where** to find modules and **what** are the contents of
the modules (where are the source files of the module, what is the module 
version, dependencies, ...). The latter is satsified by the module definition 
file, which every module **must** define and **needs** to be in the root 
directory of the module repository. Thus the module manager needs to know the 
location of the module repository in the file system to find the corresponding 
module definition file. This location is called the **module path**. Accordingly
a module is completely defined by it's module definition file and module path. 
The module definition file location and contents are well defined, therefore the
module manager needs only to know the module path, which is announced to the 
module manager by the `add-module-paths` command. This procedure accepts one
parameter, a list of module paths. Therefor to register modules to the module
manager, add the following lines to the `.fluent` file after the code added 
above:

```scheme
; Add the module paths.
(add-module-paths (list
  ; If the modules are in the "jkuisw" directory.
  (string-append jkuisw-path "/fscm-module-1")
  (string-append jkuisw-path "/fscm-module-2")
  ; Any other location of the module repository.
  "/absolute/path/to/fscm-module-3"
  "/absolute/path/to/fscm-module-4"
))
```

> :information_source: The `add-module-paths` command can of course be called
> multiple times. But a module can only be imported after the module path was
> added. Therefore it is not mandatory that the modules are registered in the
> `.fluent` file, but with this approach you only need to define the module
> paths once for every fluent instance.

### Finished
At this point we are finished with the module manager setup. Now you can import
other modules with the `import` command. But this and some other useful 
procedures are described in the next section.

Usage
-----
The full documentation for all procedures of the module manager can be found in
the [API documentation][api-doc]. Below are only some basic concepts for 
the usage of the module manager described.  

> :information_source: It is recommended to use the module 
> [fscm-project-manager][fscm-project-manager-repo] in conjunction with the
> module manager.

### Import modules
With the `import` command you import a module to your source file. The same 
module can be imported multiple times, the module manager will recognize this
and load the module only once (except you define it otherwise by the force 
parameter). This allows you to import modules at the beginning of all your 
source files to document the dependencies of the source file, which is good 
practice. The `import` command has two mandatory parameters, which are the 
import name and the module name. 

#### `import-name` - parameter
Each module will be loaded into it's own scheme environment, this means that the
module is encapsualted and no procedures and variables are accessible from 
outside. Therefore each module defines which procedures should be accessible by 
the user, these are named the exported public procedures. The module manager 
injects than these procedures into the environment where the `import` command is
executed, following the rule `<import name>/<export name>`. So for example the 
procedure `get-fluent-version` from the module 
[fscm-utils][fscm-utils-repo] with an import name "utils" 
would than be accessible through the name `utils/get-fluent-version`. Since the
import name is dynamic, you could import the same module with different import
names. This will load the module only once (if the same version or matching 
version), but the public procedure will be injected with different import names.
> :information_source: The module manager is a special module. It's public
> procedures have no import name and will be injected into the global scheme
> environment at initialization. Therefore the exported procedures of the
> module manager are available everywhere.

#### `module-name` - parameter
The name of the module is the same as the name of the repository, so dont't 
forget the preceding **fscm-** when importing a module.

#### Other parameters
All other parameters are optional, see the [API documentation][api-doc] 
for details. But one thing to mention is the module version. Every fluent scheme
module has a version, following the
[semantic versioning specification][semver-spec]. By default the module
manager will import the newest version of the module available. But optionally
you can import a module at a specific version or version range. A version range 
defines a range of module versions accepted for import. For example, given the
version range "1.2.\*", the import command will only load the module when it's 
major version is "1" and it's minor version is "2", but patch versions (and 
pre-release versions) will be ignored. Therefore valid module versions would be 
"1.2.0", "1.2.1", "1.2.8" and so on. With this feature you can avoid that 
incompatible versions of a module will be loaded (a change in the major version
is a breaking change, see the [semantic versioning specification][semver-spec]
for details).

#### Example
```scheme
; Import the "utils" module, assuming that the utils module path was already 
; registered to the module manager.
(import "utils" "fscm-utils")

; Using the utils module for example to split a string by a character.
(define str "abc-def-geh")
(utils/string-split str #\-)
```

### Get module manager status information
The module manager provides some commands to gather information about the
current status of it. 

#### `print-module-exports`
The command prints all exported procedures (with their exported name) of a 
module. For this you must provide the name (import-name) with which the module
was imported. 
```scheme
(print-module-exports "utils")
```

#### `print-import-tree`
The import tree shows you which modules are imported with which import name. 
Furthermore it shows you the nesting of the module environments. For example the
[fscm-file-system-ops][fscm-file-system-ops-repo] module depends on the
[fscm-utils][fscm-utils-repo] module. Thus it has one nested (child) environment
for the utils module loaded.
```scheme
(print-import-tree)
```

#### `print-modules-list`
The command prints the modules list that shows you which modules are registered 
and some further informations of the modules. This is useful to check if the
module registration works as expected.
```scheme
(print-modules-list)
```

How to Create a new Module
--------------------------
First you need to create a new git repository with `fluent scheme` language
support from the [isw-git-tools repository][isw-git-tools-repo]. To do so follow
the instructions of the [README.md][isw-git-tools-repo-readme] from the
[isw-git-tools repository][isw-git-tools-repo] to create a new repository with
`fluent scheme` language support.

> :warning: The repository (project) name must be the fluent scheme module name!

After that create the following recommended directory structure (if you haven't
already):

```
<repository-name>
|--+ doc
|--+ docker
|--+ scripts
|--+ src
+--+ submodules
```

Explanation of the directories:
* `doc` - Location for the generated API documentation (see the
  [fluent scheme language support][isw-git-tools-fluent-scheme]).
* `docker` - Location for the Dockerfiles (see the
  [fluent scheme language support][isw-git-tools-fluent-scheme]).
* `scripts` - For the script wrappers and the configuration of them (see the
  [fluent scheme language support][isw-git-tools-fluent-scheme]).
* `src` - Where the source files of this module are located (the actual
  implementation of the module).
* `submodules` - The git submodules this module depends on.

The next step is to configure the module so that the module manager knows where
to find the source code, what the current version of the module is, which
dependencies the module has and some other information about the module. This is
achieved by the module definition file. Therefore copy the
[module definition template][fscm-module-manager-module-def-template]
`moduledef.scm.template` from the
[fscm-module-manager repository][fscm-module-manager-repo] to your new
repository (module) and rename it to `moduledef.scm` (or copy the contents of
the template and save the file as `moduledef.scm`). The module definition file
must be in the root directory of your new module (repository) and must exactly
be named `moduledef.scm`, so that the module manager can find that file when
the module is registered. Open the file and configure your new module. The
comments in the module definition file should describe each possible setting and
how each setting can be configured. 

Before you start to implement the new module, consider the following
recommendations:
* Use the development workflow as described in
  [this guide][isw-git-tools-dev-workflow].
* When you use the recommended development workflow, use the `master` branch as
  the production branch and protect the branch so that only members of the
  project with a permission level of at least `Master` can push/merge to that
  branch. See the
  [GitLab protected branches documentation][gitlab-protected-branches] for
  further information.
* Take the [fscm-utils][fscm-utils-repo] and
  [fscm-file-system-ops][fscm-file-system-ops-repo] modules as examples for a
  fluent scheme module setup.
* Choose a license for the module, for example the [MIT license][mit-license].
* Document the repository in the `README.md` (what the module does, how to
  use it, examples of important features).
* Document the development conventions and how to contribute to the module in
  the `CONTRIBUTING.md` file.
* If your new module depends on another fluent scheme module, for example the
  [fscm-utils][fscm-utils-repo] module, add it as git submodule and configure it
  as dependency in the module definition file. See the
  [git submodules documentation][git-submodules] for how to work with git
  submodules. When you do this, than your module will provide it's own
  dependencies in the right version to the user and the module manager can
  automatically load that dependencies. When the dependencies of a module are
  not well defined, than it could fail to execute because of missing or wrong
  version of dependencies.
* Keep your source files short and clean. A good rule of thumb is to keep the
  number of lines per source file under 1000. If a source file needs more than
  1000 lines, than you should probably re-factor your code into more procedures
  and source files.
* Document your code along with the source code! This means documentation of
  your procedures interface (see the
  ["How to Document the Code"][isw-git-tools-fluent-scheme-doc] section in the
  [fluent scheme language support documentation][isw-git-tools-fluent-scheme])
  as well as documenting with comments along the code what it does.

Now you can start the development of the new module.



<!-- links reference -->
[api-doc]: /doc/README.md

[jkuisw-group]: https://gitlab.com/jkuisw
[isw-git-tools-repo]: https://gitlab.com/jkuisw/isw-git-tools
[isw-git-tools-repo-readme]: https://gitlab.com/jkuisw/isw-git-tools/blob/master/README.md
[isw-git-tools-fluent-scheme]: https://gitlab.com/jkuisw/isw-git-tools/tree/master/repo-scripts/fluent-scheme
[isw-git-tools-fluent-scheme-doc]: https://gitlab.com/jkuisw/isw-git-tools/tree/master/repo-scripts/fluent-scheme#how-to-document-the-code
[isw-git-tools-dev-workflow]: https://gitlab.com/jkuisw/isw-git-tools/blob/master/guides/dev-workflow.md
[fscm-module-manager-repo]: https://gitlab.com/jkuisw/fscm-module-manager
[fscm-module-manager-module-def-template]: https://gitlab.com/jkuisw/fscm-module-manager/blob/master/moduledef.scm.template
[fscm-utils-repo]: https://gitlab.com/jkuisw/fscm-utils
[fscm-file-system-ops-repo]: https://gitlab.com/jkuisw/fscm-file-system-ops
[fscm-project-manager-repo]: https://gitlab.com/jkuisw/fscm-project-manager
[setup-gitlab-access-guide]: https://gitlab.com/jkuisw/isw-git-tools/blob/master/guides/setup-gitlab-access.md

[semver-spec]: http://semver.org/
[git-submodules]: https://git-scm.com/book/en/v2/Git-Tools-Submodules
[gitlab-protected-branches]: https://docs.gitlab.com/ee/user/project/protected_branches.html#protected-branches
[mit-license]: https://choosealicense.com/licenses/mit/

<!-- images reference -->
[dot-fluent-file-structure]: /resources/dot-fluent-file-structure.png
