# Module: fscm-module-manager
This is the overview of the module documentation. In this file you can find a list with all source files of the module, a list of all exported procedures of the module and a list of all module private procedures. The name of each list entry links to the specific documentation of the source file, exported procedure or private procedure.  

## Source Files
A list of all source files of this module.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: helpers](/doc/helpers.md) | Implements some helper procedures for the module manager. |
| [:page_facing_up: import-tree](/doc/import-tree.md) | This part of the module provides procedures for interacting with the import\... |
| [:page_facing_up: module-manager](/doc/module-manager.md) | Provides the core functionality of the module mananger, which is: \n\n+ add... |
| [:page_facing_up: version-parsing](/doc/version-parsing.md) | This file implements parsing and matching of semver strings. The semver 2.0.... |

## Exported Procedures
A list of all exported procedures of this module. The procedure names are the 
internal procedure names. Normally this is the same as the exported name, but 
thy can differ. Look into the detailed procedure documentation to get the export 
name of the procedure. The procedures listed in the table below are imported 
into the global environment with their export name.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: list-module-exports](/doc/import-tree.md#list-module-exports) | Generates a list of the exports the module, identified by the import-namepar... |
| [:page_facing_up: print-module-exports](/doc/import-tree.md#print-module-exports) | Prints the names of the exported procedures the module, identified by theimp... |
| [:page_facing_up: print-import-tree](/doc/import-tree.md#print-import-tree) | Prints the module import tree. |
| [:page_facing_up: add-module-paths](/doc/module-manager.md#add-module-paths) | Adds module paths to the module manager. Every module path must be an absolu... |
| [:page_facing_up: import](/doc/module-manager.md#import) | Imports a module. The module will only be loaded once. If you want to forcea... |
| [:page_facing_up: print-modules-list](/doc/module-manager.md#print-modules-list) | Prints some information about the known modules. |
| [:page_facing_up: parse-version](/doc/version-parsing.md#parse-version) | Parses the given version or version-range string and returns the parsed data... |
| [:page_facing_up: is-version-range-matching?](/doc/version-parsing.md#is-version-range-matching) | Checks if the version matches the version-range. |

## Private Procedures
A list of all private procedures of this module. This procedures are not accessible from outside the module.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: append-to-list](/doc/helpers.md#append-to-list) | Appends a value to the list. |
| [:page_facing_up: map-get-value](/doc/helpers.md#map-get-value) | Retrieves the value of a key in a key value map list. |
| [:page_facing_up: map-set-value](/doc/helpers.md#map-set-value) | Sets the value of a key in a key value map list. |
| [:page_facing_up: file-content-to-list](/doc/helpers.md#file-content-to-list) | Reads the content of the given file (path) and converts it to an object list... |
| [:page_facing_up: get-source-files-of-directory](/doc/helpers.md#get-source-files-of-directory) | Retrieves all source files of a directories. Source files are all files with... |
| [:page_facing_up: load-source-files-of-directory](/doc/helpers.md#load-source-files-of-directory) | Loads all source files in the given directory. |
| [:page_facing_up: create-module-enviroment](/doc/helpers.md#create-module-enviroment) | Creates a new module environment. |
| [:page_facing_up: set-module-loaded](/doc/helpers.md#set-module-loaded) | Sets the loaded flag of a module to true. |
| [:page_facing_up: get-module-settings](/doc/helpers.md#get-module-settings) | Retrieves the settings of a module (defined by the parameters name andoption... |
| [:page_facing_up: is-known-module?](/doc/helpers.md#is-known-module) | Checks if the module defined by the given name and version-range is known by... |
| [:page_facing_up: traverse-import-tree](/doc/import-tree.md#traverse-import-tree) | Traverses the import tree and executes actions on specific positions andstat... |
| [:page_facing_up: load-module](/doc/module-manager.md#load-module) | Actually loads a module, which means: Find the module in the modules list,re... |
| [:page_facing_up: read-dependent-modules](/doc/module-manager.md#read-dependent-modules) | Reads the dependencies (module definitions) of a module. |
| [:page_facing_up: read-module-definition](/doc/module-manager.md#read-module-definition) | This procedure reads the module definition of the given module path and it\'... |
| [:page_facing_up: refresh-modules-list](/doc/module-manager.md#refresh-modules-list) | Refreshes the modules list, which means: Loop through all module paths, read... |
| [:page_facing_up: import-interceptor](/doc/module-manager.md#import-interceptor) | Passes all parameters to the import procedure. This procedure is used tointe... |
| [:page_facing_up: calc-level-decision](/doc/version-parsing.md#calc-level-decision) | Calculates a decision according to the given versions. Each entry of theleve... |
| [:page_facing_up: calc-pre-release-decision](/doc/version-parsing.md#calc-pre-release-decision) | Calculates the decision for the two pre-release versions. Each entry of thep... |
| [:page_facing_up: is-comparator-matching?](/doc/version-parsing.md#is-comparator-matching) | Checks if the comparator matches the given version. |

