:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: version-parsing.scm
This file implements parsing and matching of semver strings. The semver 2.0.0
specification for semantic versioning strings is fully implemented.  
  -> see: http://semver.org/  
For version string matching the version ranges are used.  
  -> see: https://docs.npmjs.com/misc/semver  
Except for "Advanced Range Syntax", all version-range features are
supported and implemented.

:arrow_right: [Go to source code of this file.](/src/version-parsing.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: parse-version](#parse-version) | Parses the given version or version-range string and returns the parsed data... |
| [:page_facing_up: is-version-range-matching?](#is-version-range-matching) | Checks if the version matches the version-range. |

## Private Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: calc-level-decision](#calc-level-decision) | Calculates a decision according to the given versions. Each entry of theleve... |
| [:page_facing_up: calc-pre-release-decision](#calc-pre-release-decision) | Calculates the decision for the two pre-release versions. Each entry of thep... |
| [:page_facing_up: is-comparator-matching?](#is-comparator-matching) | Checks if the comparator matches the given version. |

## Procedure Documentation

### parse-version

#### Syntax
```scheme
(parse-version version [print-parse-result])
```

:arrow_right: [Go to source code of this procedure.](/src/version-parsing.scm#L146)

#### Export Name
`parse-version`

#### Description
Parses the given version or version-range string and returns the parsed data
in a list of the following format:  
The parsed data is a list of or logical linkage lists, like:  
```scheme
(list  
  (list ...) ; or logical linkage list  
  (list ...) ; or logical linkage list  
  ...  
)  
```
If a version-range string is passed, than each comparator or comparators
joined by spaces (= "and logical linkage") which is linked through "||"
characters (= "or logical linkage") creates a new "or logical linkage
list". If a version string is passed than only one "or logical linkage list"
is created, otherwise it would be a version-range string.
Each "or logical linkage list" is a list of "and logical linkage lists",
which are the comparators seperated by spaces. Again for a version string
there can only exist one "and logical linkage list", otherwise it would be a
version range. So a "or logical linkage list" looks like:  
```scheme
(list  
  (list ...) ; and logical linkage list  
  (list ...) ; and logical linkage list  
  ...  
)  
```
A "and logical linkage list" is a parsed comparator which is implemented as a
key value map with the following keys:  
+ `'operator` - The comparison operator, which can be one of the
  following values: `""`, `">"`, `">="`, `"<"`, `"<="`, `"="`  
  Whereby `""` is equal to `"="`. For a version string (not a version-range) this
  is always `""`.
+ `'major` - The major version as string (can also be `"*"`).
+ `'minor` - The minor version as string (can also be `"*"`).
+ `'patch` - The patch version as string (can also be `"*"`).
+ `'pre-release` - A list of strings, which are the identifiers of the
  pre-release version.
+ `'metadata` - A list of strings, which are the identifiers of the
  build metadata.

For the specification of a version, see: http://semver.org/  
For the definition of a version-range, comparator, operator and other
keywords, see: https://docs.npmjs.com/misc/semver  
> NOTE: The "Advanced Range Syntax" of a version-range is not supported.

#### Parameters
##### `version`  
The version or version-range string to parse.

##### `print-parse-result`  
_Attributes: optional, default: `#f`_  
True when the parse result
should be printed, false otherwise.


#### Returns
The parsed version / version-range as list, see above.

-------------------------------------------------
### calc-level-decision

#### Syntax
```scheme
(calc-level-decision v-val r-val level-decisions)
```

:arrow_right: [Go to source code of this procedure.](/src/version-parsing.scm#L570)

#### Description
Calculates a decision according to the given versions. Each entry of the
level-decisions list contains a decision result for an specific boolean
operator. The boolean operators are applied to the v-val and r-val. The result
of the operator that evaluates to true is returned.

#### Parameters
##### `v-val`  
The version value.

##### `r-val`  
The version range value.

##### `level-decisions`  
The list of decisions for the current level. This is one
entry of the version decision matrix and the list contains decision
results for the following operations in the following order:
  1. operator: `"<"`, lesser than
  2. operator: `"="`, equal to
  3. operator: `">"`, greater than
  4. operator: `"*"`, wildcard


#### Returns
The result of the decision.

-------------------------------------------------
### calc-pre-release-decision

#### Syntax
```scheme
(calc-pre-release-decision v-val r-val pre-release-decisions)
```

:arrow_right: [Go to source code of this procedure.](/src/version-parsing.scm#L612)

#### Description
Calculates the decision for the two pre-release versions. Each entry of the
pre-release-decisions list contains a decision result for an specific boolean
operator. The `'*` operator is ignored, because it is not applicable to a
pre-release version. The precedence between the two given pre-release versions
will be calculated according to the semantic versioning specification 2.0.0,
see: http://semver.org/#spec-item-11
With the precedence result the decision can be returned from the
pre-release-decisions list.

#### Parameters
##### `v-val`  
The version value.

##### `r-val`  
The version range value.

##### `pre-release-decisions`  
The list of decisions for the pre-release level.
This is one entry of the version decision matrix and the list contains
decision results for the following operations in the following order:
  1. operator: `"<"`, lesser than
  2. operator: `"="`, equal to
  3. operator: `">"`, greater than
  4. operator: `"*"`, wildcard (ignored by pre-release versions)


#### Returns
The result of the decision.

-------------------------------------------------
### is-comparator-matching?

#### Syntax
```scheme
(is-comparator-matching? version comparator)
```

:arrow_right: [Go to source code of this procedure.](/src/version-parsing.scm#L671)

#### Description
Checks if the comparator matches the given version.

#### Parameters
##### `version`  
A parsed version, see the description of the `parse-version`
procedure.

##### `comparator`  
A parsed comparator, see the description of the `parse-version`
procedure.


#### Returns
True if comparator matches the version, false otherwise.

-------------------------------------------------
### is-version-range-matching?

#### Syntax
```scheme
(is-version-range-matching? version version-range)
```

:arrow_right: [Go to source code of this procedure.](/src/version-parsing.scm#L782)

#### Export Name
`is-version-range-matching?`

#### Description
Checks if the version matches the version-range.

#### Parameters
##### `version`  
A specific version string.

##### `version-range`  
A version range string.


#### Returns
True if the version matches the version-range, false otherwise.

