:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: import-tree.scm
This part of the module provides procedures for interacting with the import
tree. The import tree tracks the relations between all imported modules, their
import names and their "child" modules. With the import tree the module 
manager knows which module is imported with which import name and to which 
module environment.

:arrow_right: [Go to source code of this file.](/src/import-tree.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: list-module-exports](#list-module-exports) | Generates a list of the exports the module, identified by the import-namepar... |
| [:page_facing_up: print-module-exports](#print-module-exports) | Prints the names of the exported procedures the module, identified by theimp... |
| [:page_facing_up: print-import-tree](#print-import-tree) | Prints the module import tree. |

## Private Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: traverse-import-tree](#traverse-import-tree) | Traverses the import tree and executes actions on specific positions andstat... |

## Procedure Documentation

### traverse-import-tree

#### Syntax
```scheme
(traverse-import-tree settings actions)
```

:arrow_right: [Go to source code of this procedure.](/src/import-tree.scm#L94)

#### Description
Traverses the import tree and executes actions on specific positions and
states of the import tree. See the actions parameter for further information.
The procedure can operate in two different modes:  
+ `"find"` mode - When in the settings only the import-name and import-path
  keys are passed. These twoe settings are mandatory in this mode. Than the
  procedure will search for the module, identified by the import-path and
  import-name. When found, than the module entry in the import tree will be
  passed to the `"found-module-action"`. The actions `"add-entry-action"`,
  `"matching-imported-action"` and `"other-imported-action"` will be ignored in
  this mode.
+ `"add"` mode - When all settings are passed. In this mode all settings are
  mandatory. Then the procedure will add a new entry for the module to the
  import tree (when the action `"add-entry-action"` returns true, see below).
  The action `"found-module-action"` will be ignored in this mode.

#### Parameters
##### `settings`  
A settings key value pair list which must have the following
entries:  
+ `import-path` - The import path of the module.
+ `import-name` - The import name of the module.
+ `module-name` - The module name.
+ `module-version` - The module version.
+ `version-range` - A valid version range for the module.

##### `actions`  
A key value pair list with different actions. An action is a
procedure that will be called on specific positions and states of the
import tree. If a action is not passed, than it will be ignored. The
following actions are supported:  
+ `path-not-found-action` - Will be called when the import path could not
  be found in the import tree. The procedure will be called with the
  following parameters:  
  + `level-tree` - The remaining entries of the current subtree.
  + `path` - The remaining entries of the import path.
+ `add-entry-action` - Called when the position for a new entry is found.
  The entry is than added only when the procedure returns true. If false
  is returned by the procedure, than the entry will not be added. The
  procedure will be called with the following parameters:
  + `level-tree` - The remaining entries of the current subtree. If an
    empty list, than this will be the first entry in the import tree.
    If not, than there should be only one entry in the remaining
    subtree.
  + `path` - The remaining entries of the import path. Should be an
    empty list at this point.
+ `matching-imported-action` - Will be called if a matching entry,
  according to the settings passed, is already in the import tree. The
  procedure will be called with the following parameters:
  + `entry` - The entry of the already imported module in the import
    tree.
+ `other-imported-action` - Will be called if the import name is already
  used by another loaded module, which does not match with the settings
  passed. The procedure will be called with the following parameters:
  + `entry` - The entry of the already imported module in the import
    tree.
+ `found-module-action` - Will be called when in `"find"` mode and the
  module, identified by the import-name and import-path, is found. The
  procedure will be called with the following parameters:
  + `entry` - The import tree entry of the found module.



-------------------------------------------------
### list-module-exports

#### Syntax
```scheme
(list-module-exports import-name [import-path])
```

:arrow_right: [Go to source code of this procedure.](/src/import-tree.scm#L274)

#### Export Name
`list-module-exports`

#### Description
Generates a list of the exports the module, identified by the import-name
parameter, has exported.

#### Parameters
##### `import-name`  
The import name to which the module was imported.

##### `import-path`  
_Attributes: optional, default: `'()`_  
The import path to which the module
with the passed import-name was passed. Default is the global import path.


#### Returns
A list of symbols which are the names of the exported procedures.

-------------------------------------------------
### print-module-exports

#### Syntax
```scheme
(print-module-exports import-name [import-path])
```

:arrow_right: [Go to source code of this procedure.](/src/import-tree.scm#L387)

#### Export Name
`print-module-exports`

#### Description
Prints the names of the exported procedures the module, identified by the
import-name parameter, has exported.

#### Parameters
##### `import-name`  
The import name to which the module was imported.

##### `import-path`  
_Attributes: optional, default: `'()`_  
The import path to which the module
with the passed import-name was passed. Default is the global import path.



-------------------------------------------------
### print-import-tree

#### Syntax
```scheme
(print-import-tree)
```

:arrow_right: [Go to source code of this procedure.](/src/import-tree.scm#L415)

#### Export Name
`print-import-tree`

#### Description
Prints the module import tree.


