:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: helpers.scm
Implements some helper procedures for the module manager.

:arrow_right: [Go to source code of this file.](/src/helpers.scm)

## Private Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: append-to-list](#append-to-list) | Appends a value to the list. |
| [:page_facing_up: map-get-value](#map-get-value) | Retrieves the value of a key in a key value map list. |
| [:page_facing_up: map-set-value](#map-set-value) | Sets the value of a key in a key value map list. |
| [:page_facing_up: file-content-to-list](#file-content-to-list) | Reads the content of the given file (path) and converts it to an object list... |
| [:page_facing_up: get-source-files-of-directory](#get-source-files-of-directory) | Retrieves all source files of a directories. Source files are all files with... |
| [:page_facing_up: load-source-files-of-directory](#load-source-files-of-directory) | Loads all source files in the given directory. |
| [:page_facing_up: create-module-enviroment](#create-module-enviroment) | Creates a new module environment. |
| [:page_facing_up: set-module-loaded](#set-module-loaded) | Sets the loaded flag of a module to true. |
| [:page_facing_up: get-module-settings](#get-module-settings) | Retrieves the settings of a module (defined by the parameters name andoption... |
| [:page_facing_up: is-known-module?](#is-known-module) | Checks if the module defined by the given name and version-range is known by... |

## Procedure Documentation

### append-to-list

#### Syntax
```scheme
(append-to-list lst value)
```

:arrow_right: [Go to source code of this procedure.](/src/helpers.scm#L10)

#### Description
Appends a value to the list.

#### Parameters
##### `lst`  
The list to which the value to append.

##### `value`  
The value to append.



-------------------------------------------------
### map-get-value

#### Syntax
```scheme
(map-get-value key-value-list key)
```

:arrow_right: [Go to source code of this procedure.](/src/helpers.scm#L29)

#### Description
Retrieves the value of a key in a key value map list.

#### Parameters
##### `key-value-list`  
The key value map list.

##### `key`  
The key from which to read the value.


#### Returns
The value of the key.

-------------------------------------------------
### map-set-value

#### Syntax
```scheme
(map-set-value key-value-list key value)
```

:arrow_right: [Go to source code of this procedure.](/src/helpers.scm#L57)

#### Description
Sets the value of a key in a key value map list.

#### Parameters
##### `key-value-list`  
The key value map list.

##### `key`  
The key in the map, where to write the value.

##### `value`  
The value to write.



-------------------------------------------------
### file-content-to-list

#### Syntax
```scheme
(file-content-to-list inputfile)
```

:arrow_right: [Go to source code of this procedure.](/src/helpers.scm#L93)

#### Description
Reads the content of the given file (path) and converts it to an object list.
Objects are interpreted as strings seperated by whitespace.

#### Parameters
##### `inputfile`  
The file from which to read.


#### Returns
A list of read objects.

-------------------------------------------------
### get-source-files-of-directory

#### Syntax
```scheme
(get-source-files-of-directory directory)
```

:arrow_right: [Go to source code of this procedure.](/src/helpers.scm#L122)

#### Description
Retrieves all source files of a directories. Source files are all files with
the file extension `".scm"`. The developer has to guarantee that all source
in the directory are valid fluent scheme files.

#### Parameters
##### `directory`  
The directory in which to search for source files.


#### Returns
A list of source file names in the directory. If no files were found
an empty list is returned.

-------------------------------------------------
### load-source-files-of-directory

#### Syntax
```scheme
(load-source-files-of-directory directory environment)
```

:arrow_right: [Go to source code of this procedure.](/src/helpers.scm#L162)

#### Description
Loads all source files in the given directory.

#### Parameters
##### `directory`  
The directory of which to load the source files.

##### `environment`  
The environment where to load the files.



-------------------------------------------------
### create-module-enviroment

#### Syntax
```scheme
(create-module-enviroment module-import-path import-name parent-environments name objects-to-inject export-proc)
```

:arrow_right: [Go to source code of this procedure.](/src/helpers.scm#L211)

#### Description
Creates a new module environment.

#### Parameters
##### `module-import-path`  
The import path of the module.

##### `import-name`  
The import name of the module for which to create the
environment.

##### `parent-environments`  
The parent environments of the module environment to
create (list).

##### `name`  
The module name.

##### `objects-to-inject`  
The objects to inject to the new environment as name
value pair list. The object names must be symbols. e.g.:
```scheme  
(list
  (list name1 value1)
  (list name2 value2)
  ...
)
```

##### `export-proc`  
The export procedure for the module environment, must be a
procedure. The procedure will be added to the environment with the name
`"export"` and must accept one parameter:  
+ `exports` - A name value pair list with the procedures of the module to
  export. The name must be a symbol and the value a procedure. Normally
  the name should be the same as the procedure name, but this is not
  mandatory. e.g.:  

  ```scheme
  (list
    (list 'proc1 proc1)
    (list 'proc2 proc2)
    ...
  )
  ```


#### Returns
The newly created module environment.

-------------------------------------------------
### set-module-loaded

#### Syntax
```scheme
(set-module-loaded name version)
```

:arrow_right: [Go to source code of this procedure.](/src/helpers.scm#L266)

#### Description
Sets the loaded flag of a module to true.

#### Parameters
##### `name`  
The module name.

##### `version`  
The specific version of the module. Not a version range!



-------------------------------------------------
### get-module-settings

#### Syntax
```scheme
(get-module-settings name [version-range])
```

:arrow_right: [Go to source code of this procedure.](/src/helpers.scm#L300)

#### Description
Retrieves the settings of a module (defined by the parameters name and
optional version), which is an entry of the modules list.

#### Parameters
##### `name`  
The module name.

##### `version-range`  
_Attributes: optional, default: `""`_  
The version range to match against.


#### Returns
The settings of a module.

-------------------------------------------------
### is-known-module?

#### Syntax
```scheme
(is-known-module? name version-range)
```

:arrow_right: [Go to source code of this procedure.](/src/helpers.scm#L364)

#### Description
Checks if the module defined by the given name and version-range is known by
the module manager (is in the modules list).

#### Parameters
##### `name`  
The name of the module.

##### `version-range`  
The version range to match against.


#### Returns
True if the module is known (the version-range matches with a known
one), false otherwise.

