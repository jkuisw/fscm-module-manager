:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: module-manager.scm
Provides the core functionality of the module mananger, which is:  

+ adding module paths to find fluent scheme modules
+ search for modules in the added module paths
+ read the module definitions of the modules
+ load modules when requested and needed
+ import modules to source files
+ print information about the known and loaded modules

:arrow_right: [Go to source code of this file.](/src/module-manager.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: add-module-paths](#add-module-paths) | Adds module paths to the module manager. Every module path must be an absolu... |
| [:page_facing_up: import](#import) | Imports a module. The module will only be loaded once. If you want to forcea... |
| [:page_facing_up: print-modules-list](#print-modules-list) | Prints some information about the known modules. |

## Private Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: load-module](#load-module) | Actually loads a module, which means: Find the module in the modules list,re... |
| [:page_facing_up: read-dependent-modules](#read-dependent-modules) | Reads the dependencies (module definitions) of a module. |
| [:page_facing_up: read-module-definition](#read-module-definition) | This procedure reads the module definition of the given module path and it\'... |
| [:page_facing_up: refresh-modules-list](#refresh-modules-list) | Refreshes the modules list, which means: Loop through all module paths, read... |
| [:page_facing_up: import-interceptor](#import-interceptor) | Passes all parameters to the import procedure. This procedure is used tointe... |

## Procedure Documentation

### load-module

#### Syntax
```scheme
(load-module import-name name [version-range] [parent-env])
```

:arrow_right: [Go to source code of this procedure.](/src/module-manager.scm#L44)

#### Description
Actually loads a module, which means: Find the module in the modules list,
retrieve the module settings. Find all source files in the module path and
module source files. Finally load all source files and set the loaded flag of
the module to true.

#### Parameters
##### `import-name`  
The import name of the module, must be a symbol.

##### `name`  
The module name, must be a string.

##### `version-range`  
_Attributes: optional, default: `""`_  
The version range to match against.

##### `parent-env`  
_Attributes: optional, default: `user-initial-environment`_  
The parent
environment of the environment where the module will be loaded to.



-------------------------------------------------
### read-dependent-modules

#### Syntax
```scheme
(read-dependent-modules basedir dependencies dep-path)
```

:arrow_right: [Go to source code of this procedure.](/src/module-manager.scm#L255)

#### Description
Reads the dependencies (module definitions) of a module.

#### Parameters
##### `basedir`  
The path to the directory of the module which dependencies should
be read.

##### `dependencies`  
The list of dependencies to read, see the file
"moduledef.scm.template" for the list format of this parameter.

##### `dep-path`  
The dependency path, see the description of the procedure
"read-module-definition" for an explanation of this parameter.



-------------------------------------------------
### read-module-definition

#### Syntax
```scheme
(read-module-definition path [dependency-path])
```

:arrow_right: [Go to source code of this procedure.](/src/module-manager.scm#L334)

#### Description
This procedure reads the module definition of the given module path and it's
dependencies.

#### Parameters
##### `path`  
The module path, which contains the module definition file.

##### `dependency-path`  
_Attributes: optional, default: `'()`_  
The dependency path for the
module to load. This is a list of name - version - pairs of the modules
that are depending on this module. Example:  
```scheme
(list 
  (list "module1" "2.5.3") 
  (list "module2" "1.1.3")
)
```
This means: The first call of this procedure was to read the module
definition of the module "module1", which depends on the module "module2".
So the procedure is called to read the module definition of the module
"module2", which dependes on the current module, so the procedure is
called to read the module definition of the current module.



-------------------------------------------------
### refresh-modules-list

#### Syntax
```scheme
(refresh-modules-list)
```

:arrow_right: [Go to source code of this procedure.](/src/module-manager.scm#L564)

#### Description
Refreshes the modules list, which means: Loop through all module paths, read
the module definition and save the settings. The list will only be refreshed
if the module paths have changed, specifically if the flag
"should-refresh-modules-list" is set to true. This is the case if a
path was added by the "add-module-paths" procedure and there was no refresh
after that.


-------------------------------------------------
### add-module-paths

#### Syntax
```scheme
(add-module-paths paths-list)
```

:arrow_right: [Go to source code of this procedure.](/src/module-manager.scm#L597)

#### Export Name
`add-module-paths`

#### Description
Adds module paths to the module manager. Every module path must be an absolute
path to the parent folder of the module, which contains the module definition
file, which must have exactly the name "moduledef.scm". See the template
"moduledef.scm.template" for how to write the module definition file.

#### Parameters
##### `paths-list`  
A list of module paths to add.



-------------------------------------------------
### import-interceptor

#### Syntax
```scheme
(import-interceptor parent-module-name module-env import-name module-name [version-range] [force-load] [parent-environment])
```

:arrow_right: [Go to source code of this procedure.](/src/module-manager.scm#L659)

#### Description
Passes all parameters to the import procedure. This procedure is used to
intercept calls to the import procedure. In an module environment the call to
import is intercepted by this procedure by assigning it to the 'import symbol
in the module environment of the module this import was called from. It
ignores an optional passed environment to the import call and replaces it with
the passed module environment.

#### Parameters
##### `parent-module-name`  
The name of the parent module.

##### `module-env`  
The module environment passed to the import procedure.

##### `import-name`  
Will be passed to the import procedure, see the import
procedure for the parameter description.

##### `module-name`  
Will be passed to the import procedure, see the import
procedure for the parameter description.

##### `version-range`  
_Attributes: optional, default: `""`_  
Will be passed to the import
procedure, see the import procedure for the parameter description.

##### `force-load`  
_Attributes: optional, default: `#f`_  
Will be passed to the import
procedure, see the import procedure for the parameter description.

##### `parent-environment`  
_Attributes: optional, default: `user-initial-environment`_  
If 
passed, will be ignored and replaced by the module-env parameter in the 
import call.



-------------------------------------------------
### import

#### Syntax
```scheme
(import import-name module-name [version-range] [force-load] [parent-environment])
```

:arrow_right: [Go to source code of this procedure.](/src/module-manager.scm#L718)

#### Export Name
`import`

#### Description
Imports a module. The module will only be loaded once. If you want to force
a reload, you have to pass true to the force-load parameter.
> NOTE: The path to the module has to be added by the procedure
> "add-module-paths" before you can import the module.

#### Parameters
##### `import-name`  
The import name of the module. Each exported procedure of the
module will be loaded into the environment following the syntax:
`<import-name>/<procedure-name>`, e.g.:  
+ import-name = utils  
+ procedure-name = get-unique-file-path  
+ => `utils/get-unique-file-path`

##### `module-name`  
The name of the module to load.

##### `version-range`  
_Attributes: optional, default: `""`_  
The version of the module to import.
If the parameter is given as a version range, than the newest module which
satisfies the version-range will be imported. Examples:  
+ `"2.3.4"` - specific version, only the module with exactly this version
  will be imported.
+ `">=1.2.2"` - The newest module with a version bigger or equal to
  `"1.2.2"` will be imported. Other possible operators are: `">"`, `"<"`,
  `"<="`, `"="` If no version operator is given, `"="` is assumed.
+ `"2.3.*"` - The newest module which majo version is 2 and minor version
  is 3 will be imported. The patch version will be ignored, because
  of the wildcard `"*"` (instead of `"*"`, `"x"` or `"X"` can also be 
  used). The wildcard can also be used for the minor and/or major version.

##### `force-load`  
_Attributes: optional, default: `#f`_  
By default a module will only be loaded
once, but if this parameter is true, than the module will be loaded,
regardless if it is loaded already.

##### `parent-environment`  
_Attributes: optional, default: `user-initial-environment`_  
The
environment from which the module environment should inherit.



-------------------------------------------------
### print-modules-list

#### Syntax
```scheme
(print-modules-list)
```

:arrow_right: [Go to source code of this procedure.](/src/module-manager.scm#L775)

#### Export Name
`print-modules-list`

#### Description
Prints some information about the known modules.


